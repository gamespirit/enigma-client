import { TASK_FINISHED, TASK_STARTED } from "./sagaActions";
import { TaskState } from "./sagaReducer";

export interface TaskStartAciton extends TaskState {
    type: typeof TASK_STARTED;
}

export interface TaskFinishedAction {
    type: typeof TASK_FINISHED;
    id: number;
}

export type SagaAcitonTypes = TaskStartAciton | TaskFinishedAction;
