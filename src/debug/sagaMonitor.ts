import Color from "color";
import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { Saga, SagaMonitor } from "redux-saga";
import thunk from "redux-thunk";
import { TASK_FINISHED, TASK_STARTED } from "./sagaActions";
import { TaskFinishedAction, TaskStartAciton } from "./sagaActionTypes";
import { sagaReducer } from "./sagaReducer";

export const sagaStore = createStore(sagaReducer, composeWithDevTools(applyMiddleware(thunk)));

function effectTriggered(options: {
    effectId: number;
    parentEffectId: number;
    label?: string | undefined;
    effect: any;
}) {
    const color = Color("#98ffaf")
        .rotate(Math.floor(Math.random() * 360))
        .hex();
    switch (options.effect.type) {
        case "CALL":
        case "FORK": {
            sagaStore.dispatch<TaskStartAciton>({
                type: TASK_STARTED,
                name: options.effect.payload.fn.name,
                id: options.effectId,
                parentId: options.parentEffectId,
                effectType: "fn",
                color,
            });
            break;
        }
        case "TAKE": {
            sagaStore.dispatch<TaskStartAciton>({
                type: TASK_STARTED,
                name: options.effect.payload.channel ? "channel" : options.effect.payload.pattern,
                id: options.effectId,
                parentId: options.parentEffectId,
                effectType: "take",
                color,
            });
            break;
        }
        case "PUT": {
            // dont register
            break;
        }
        default:
            sagaStore.dispatch<TaskStartAciton>({
                type: TASK_STARTED,
                name: options.effect.type,
                id: options.effectId,
                parentId: options.parentEffectId,
                effectType: "fn",
                color,
            });
    }
}

function effectResolved(id: number, result: any) {
    if (result && result.toPromise) {
        (result.toPromise() as Promise<any>)
            // .then(
            //     (taskResult) => {
            //         console.log("then result " + taskResult);
            //         // if (result.isCancelled()) effectCancelled(effectId);
            //         // else effectResolved(effectId, taskResult);
            //     },
            //     (taskError) => {
            //         console.log("then error " + taskError);
            //     }
            // )
            // .catch((error) => {
            //     console.log("catch " + error);
            //     effectFinished(id);
            // })
            .finally(() => {
                effectFinished(id);
            });
    }
    if (result && result.isRunning && result.isRunning()) {
        return;
    }
    const effect = sagaStore.getState()[id];
    if (effect) {
        sagaStore.dispatch<TaskFinishedAction>({
            type: TASK_FINISHED,
            id,
        });
    }
}

function rootSagaStarted(options: { effectId: number; saga: Saga<any[]>; args: any[] }) {
    const color = Color("#98ffaf")
        .rotate(Math.floor(Math.random() * 360))
        .hex();
    sagaStore.dispatch<TaskStartAciton>({
        type: TASK_STARTED,
        name: options.saga.name,
        id: options.effectId,
        parentId: 0,
        effectType: "fn",
        color,
    });
}

function effectFinished(id: number) {
    sagaStore.dispatch<TaskFinishedAction>({
        type: TASK_FINISHED,
        id,
    });
}

export const sagaMonitor: SagaMonitor = {
    // actionDispatched: console.log,
    effectCancelled: effectFinished,
    effectRejected: effectFinished,
    effectResolved,
    effectTriggered,
    rootSagaStarted,
};
