import produce from "immer";
import { Reducer } from "redux";
import { TASK_FINISHED, TASK_STARTED } from "./sagaActions";
import { SagaAcitonTypes } from "./sagaActionTypes";

export interface TaskState {
    id: number;
    parentId: number;
    name: string;
    color: string;
    effectType: "take" | "fn";
}

export interface SagaState {
    [effectId: string]: TaskState;
}

const initialState: SagaState = {};

export const sagaReducer: Reducer<SagaState, SagaAcitonTypes> = produce(
    (draft: SagaState, action: SagaAcitonTypes) => {
        switch (action.type) {
            case TASK_STARTED: {
                draft[action.id] = action;
                break;
            }
            case TASK_FINISHED: {
                delete draft[action.id];
                break;
            }
        }
    },
    initialState
);
