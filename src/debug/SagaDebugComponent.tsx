import React, { useState } from "react";
import { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { SagaState, TaskState } from "./sagaReducer";
import "./SagaDebugComponent.css";

const connector = connect((state: SagaState) => ({
    effectsById: state,
}));

interface TaskNode {
    id: number;
    name: string;
    children: TaskNode[];
    color: string;
    take?: string;
}

type Props = ConnectedProps<typeof connector>;

const SagaTaskComponent: FC<{ task: TaskNode; borderColor?: string }> = ({
    task,
    borderColor,
    children,
}) => {
    return (
        <div className="saga-task">
            <div className="task-name">
                {task.name}
                {task.take && ` [${task.take}]`}
            </div>
            <div className="sub-tasks" style={{ color: task.color }}>
                <div className="scope-pole" style={{ borderColor: borderColor }}></div>
                {task.children.map((subtask) => (
                    <SagaTaskComponent task={subtask} borderColor={task.color} key={subtask.id} />
                ))}
            </div>
        </div>
    );
};

const buildTaskTree = (
    tasks: TaskState[],
    parent: TaskState,
    parentNode?: TaskNode
): TaskNode[] => {
    const children: TaskNode[] = [];
    for (const current of tasks) {
        if (current.parentId === parent.id) {
            if (parentNode && current.effectType === "take") {
                parentNode.take = current.name;
            } else {
                const childNode: TaskNode = {
                    name: current.name,
                    id: current.id,
                    children: [],
                    color: current.color,
                };
                childNode.children = buildTaskTree(tasks, current, childNode);
                children.push(childNode);
            }
        }
    }
    return children;
};

const getRootTask = (tasks: TaskState[]): TaskState | undefined => {
    if (tasks.length) {
        let root: TaskState = tasks[0];
        for (const effect of tasks) {
            if (effect.id === root.parentId) {
                root = effect;
            }
        }
        return root;
    }
    return;
};

const SagaDebugComponent: FC<Props> = ({ effectsById }) => {
    const [open, setOpen] = useState(false);
    const effects = Object.values(effectsById);
    const rootTaskState = getRootTask(effects);
    const rootTaskNode: TaskNode = rootTaskState
        ? {
              children: buildTaskTree(effects, rootTaskState),
              name: rootTaskState.name,
              id: rootTaskState.id,
              color: rootTaskState.color,
          }
        : {
              children: [],
              name: "root",
              id: 0,
              color: "#fff",
          };
    return (
        <div className="saga-debug-window" onClick={() => setOpen(!open)}>
            {open && <SagaTaskComponent task={rootTaskNode} borderColor={"#FFFFFF"} />}
        </div>
    );
};

export default connector(SagaDebugComponent);
