import { GameState } from "./gameStateTypes";
import * as actions from "./gameActions";
import { GameScreen } from "./gameScreens";
import { MiniGameState } from "./mini-games/miniGameStateTypes";

export interface JoinGameAction {
    type: typeof actions.JOIN_GAME;
    gameId: string;
}

export interface LeaveGameAction {
    type: typeof actions.LEAVE_GAME;
}

export interface ShowScreenAciton {
    type: typeof actions.SHOW_SCREEN;
    screen: GameScreen;
}

export interface UpdateGameDataAction {
    type: typeof actions.UPDATE_GAME_STATE;
    gameState: GameState;
}

export interface JoinGameRejectedAction {
    type: typeof actions.JOIN_GAME_REJECTED;
    reason: string;
}

export interface UpdateMiniGameAction {
    type: typeof actions.UPDATE_MINI_GAME_STATE;
    miniGameState: Partial<MiniGameState>;
}

export interface StartMiniGameAction {
    type: typeof actions.START_MINI_GAME;
    miniGameState: MiniGameState;
}

export interface StopMiniGameAction {
    type: typeof actions.STOP_MINI_GAME;
}

export type GameActionTypes =
    | ShowScreenAciton
    | JoinGameAction
    | JoinGameRejectedAction
    | UpdateGameDataAction
    | UpdateMiniGameAction
    | StartMiniGameAction
    | StopMiniGameAction
    | LeaveGameAction;
