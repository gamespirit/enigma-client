import React, { FC } from "react";
import "./TurnInfo.css";
import animals from "../../store/animals";

interface Props {}

const TurnInfo: FC<Props> = (props) => {
    const randomIndex = () => Math.floor(Math.random() * animals.length);
    const info = {
        color: "primary",
        actions: [true, false, false],
        icon: animals[randomIndex()].icon,
        turn: {
            who: "me",
            what: "Choose a letter or reveal a symbol!",
        },
    };
    return (
        <div className={`turn-info ${info.color}`}>
            <div className="actions">
                {info.actions.map((used) => (
                    <span
                        className={`action icon sprite-star-${used ? "outline-" : ""}${info.color}`}
                    ></span>
                ))}
            </div>
            {info.turn.who === "me" ? <div className="instructions">{info.turn.what}</div> : null}
            {info.turn.who !== "me" ? (
                <div className="others">
                    <span className="team-icon">{info.icon}</span>
                    <span className="who">{info.turn.who}</span>
                    <span className="what">{info.turn.what}</span>
                </div>
            ) : null}
        </div>
    );
};

export default TurnInfo;
