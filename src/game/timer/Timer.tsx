import React, { FC, useEffect, useState } from "react";
import "./Timer.css";

interface Props {}

const Timer: FC<Props> = (props) => {
    const startTime = 15;
    const [time, setTime] = useState(startTime);
    const style = { animationDuration: startTime + "s" };
    useEffect(() => {
        let tick: number;
        let tween: gsap.core.Tween;
        if (time > 0) {
            tick = window.setTimeout(() => setTime(time - 1), 1000);
        }
        return () => {
            clearTimeout(tick);
            tween && tween.kill();
        };
    }, [time]);
    return (
        <div className={"game-timer" + (time <= 10 ? " hurry" : "")}>
            <div className="timer-wrapper">
                <div className="pie filler" style={style}></div>
                <div className="pie spinner" style={style}></div>
                <div className="mask" style={style}></div>
                <div className="seconds">{time}</div>
            </div>
        </div>
    );
};

export default Timer;
