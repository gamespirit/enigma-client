import React, { FC } from "react";
import "./WinScreen.css";
import animals from "../../../../store/animals";

interface Props {}

const WinScreen: FC<Props> = () => {
    const randomIndex = () => Math.floor(Math.random() * animals.length);
    const playerName = "Arnold Schwarzenegger";
    const team = {
        icon: animals[randomIndex()].icon,
        name: "Kingwin",
        color: "primary",
    };
    const word = "Úgy szól a hangja, újradifferenciálódik";
    return (
        <div className={"win-screen centerer " + team.color}>
            <div className="win-screen-content">
                <div className="player-name">{playerName}</div>
                <div className="text-between-player-and-team">of</div>
                <div className="team">
                    <span className="team-icon">{team.icon}</span>
                    <span className="team-name">{team.name}</span>
                </div>
                <div className="win-text">guessed the word</div>
                <div className="solution">{word}</div>
            </div>
        </div>
    );
};

export default WinScreen;
