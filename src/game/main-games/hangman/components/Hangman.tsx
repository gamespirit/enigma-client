import React, { FC } from "react";
import "./Hangman.css";
import Keyboard from "./Keyboard";
import Word from "./Word";
import WinScreen from "./WinScreen";

interface Props {}

const Hangman: FC<Props> = (props) => {
    const win = false;
    return (
        <div className="hangman">
            {!win ? (
                <>
                    <Word />
                    <Keyboard />
                </>
            ) : (
                <WinScreen />
            )}
        </div>
    );
};

export default Hangman;
