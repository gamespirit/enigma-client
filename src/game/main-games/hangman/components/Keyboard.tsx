import React, { FC } from "react";
import "./Keyboard.css";

interface Props {}

const Keyboard: FC<Props> = (props) => {
    const keyboard = ["qwertzuio".split(""), "pasdfghj".split(""), "klyxcvbnm".split("")];
    return (
        <div className="keyboard">
            {keyboard.map((row, i) => (
                <div className="keyboard-row" key={i}>
                    {row.map((key, i) => (
                        <div className="key" key={i}>
                            {key}
                        </div>
                    ))}
                </div>
            ))}
        </div>
    );
};

export default Keyboard;
