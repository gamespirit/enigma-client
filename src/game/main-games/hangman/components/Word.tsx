import React, { FC, useState, useEffect, useRef } from "react";
import "./Word.css";

interface Props {}

const Word: FC<Props> = (props) => {
    const expression = "újradifferenciálódik";
    const [guess, setGuess] = useState(expression);
    const [enabled, setEnabled] = useState(false);
    const nonAlphabetChars = /[ ]|(?=[^a-zA-Z\u00C0-\u024F\u1E00-\u1EFF?])/;
    const words = guess.split(nonAlphabetChars);
    const inputEl = useRef<HTMLInputElement>(null);
    useEffect(() => {
        if (enabled) {
            inputEl.current?.focus();
        }
    }, [enabled]);
    const enable = () => {
        if (enabled) return;
        setGuess("");
        setEnabled(true);
    };
    const cancel = () => {
        if (!enabled) return;
        setGuess(expression);
        setEnabled(false);
    };
    return (
        <div className="words-container centerer">
            <div className={"words" + (enabled ? " enabled" : "")} onClick={enable}>
                {words.map((word, i) => (
                    <div className="word" key={i}>
                        {word.split("").map((letter, i) => (
                            <div
                                className={
                                    (letter.match(/[^a-zA-Z\u00C0-\u024F\u1E00-\u1EFF?]/)
                                        ? "not-alphabet "
                                        : "") + "letter"
                                }
                                key={i}
                            >
                                {letter.replace("?", " ")}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
            {enabled ? (
                <form className="solution">
                    {/* <div className="button-round primary" onClick={cancel}>
                        <span className="icon x-icon"></span>
                    </div> */}
                    <input
                        type="text"
                        className="solution-input"
                        ref={inputEl}
                        onChange={(e) => setGuess(e.target.value)}
                        onKeyDown={(e) => e.key === "Escape" && cancel()}
                        value={guess}
                        onBlur={cancel}
                    />
                    {/* <div className="button-round primary" onClick={cancel}>
                        <span className="icon sprite-tick-background"></span>
                    </div> */}
                </form>
            ) : null}
        </div>
    );
};

export default Word;
