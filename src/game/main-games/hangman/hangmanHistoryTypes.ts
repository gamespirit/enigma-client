import { MainGameRound } from "../../game-over-screen/round-history/historyDataTypes";
import { HANGMAN } from "../mainGameTypes";
export const LETTER_GUESSED = "LETTER_GUESSED";
export const LETTER_USED_BY_OTHER_TEAM = "LETTER_USED_BY_OTHER_TEAM";

interface HangmanLetterGuessed {
    type: typeof LETTER_GUESSED;
    letter: string;
    count: number;
}

interface HangmanLetterUsedByOtherTeam {
    type: typeof LETTER_USED_BY_OTHER_TEAM;
    letter: string;
}

export type HangmanSymbols = HangmanLetterGuessed | HangmanLetterUsedByOtherTeam;

export type HangmanRound = MainGameRound<typeof HANGMAN, HangmanSymbols>;
