import React, { ReactElement } from "react";
import { LETTER_GUESSED, LETTER_USED_BY_OTHER_TEAM, HangmanSymbols } from "./hangmanHistoryTypes";
import { v4 as uuid } from "uuid";

export function hangmanSymbolResolver(symbol: HangmanSymbols): ReactElement | null {
    switch (symbol.type) {
        case LETTER_GUESSED:
            return (
                <span className="guessed-letter" key={uuid()}>
                    <span className="letter">{symbol.letter}</span> x{" "}
                    <span className="count">{symbol.count}</span>
                </span>
            );
        case LETTER_USED_BY_OTHER_TEAM:
            return (
                <span className="letter-used-by-others" key={uuid()}>
                    {symbol.letter}
                </span>
            );
        default:
            return null;
    }
}
