import React, { FC } from "react";
import "./TeamIcons.css";
import animals from "../../store/animals";

interface Props {}

const TeamIcons: FC<Props> = (props) => {
    const randomIndex = () => Math.floor(Math.random() * animals.length);
    const teams = [
        {
            id: "team1",
            icon: animals[randomIndex()].icon,
            color: "primary",
            busy: false,
            winner: false,
        },
        {
            id: "team2",
            icon: animals[randomIndex()].icon,
            color: "team-red",
            busy: true,
            winner: false,
        },
        {
            id: "team3",
            icon: animals[randomIndex()].icon,
            color: "team-blue",
            busy: false,
            winner: true,
        },
    ];
    return (
        <div className="team-icons">
            {teams.map((team) => (
                <div
                    className={`team-icon color-${team.color} busy ${team.winner ? "winner" : ""}`}
                    key={team.id}
                >
                    {team.busy ? (
                        <div className={`icon-busy icon sprite-team-icon-busy-${team.color}`}></div>
                    ) : null}
                    {team.winner ? (
                        <div className="icon-star">
                            <div className={`icon sprite-star-${team.color}`}></div>
                        </div>
                    ) : null}
                    <span className="team-icon-text">{team.icon}</span>
                </div>
            ))}
        </div>
    );
};

export default TeamIcons;
