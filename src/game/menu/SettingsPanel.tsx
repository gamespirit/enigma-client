import React, { FC, useState } from "react";
import "./SettingsPanel.css";
import SettingSwitch from "./SettingSwitch";
import { SmallTitle, BorderedWindow } from "../../app/window";

interface Props {}

const TeamsPanel: FC<Props> = (props) => {
    const [help, sethelp] = useState(false);
    const [sound, setSound] = useState(true);
    const [fullScreen, setfullScreen] = useState(true);
    return (
        <div className="left settings-panel">
            <BorderedWindow colorClass="neutral">
                <SmallTitle color="neutral">Settings</SmallTitle>
                <div className="panel-content centerer">
                    <SettingSwitch
                        icon="help"
                        name="Help"
                        value={help}
                        onChange={sethelp}
                        offText="Man."
                        onText="Auto"
                    />
                    <SettingSwitch icon="sound" name="Sound" value={sound} onChange={setSound} />
                    <SettingSwitch
                        icon="full-screen"
                        name="Full-screen"
                        value={fullScreen}
                        onChange={setfullScreen}
                        onText="Auto"
                    />
                </div>
            </BorderedWindow>
        </div>
    );
};

export default TeamsPanel;
