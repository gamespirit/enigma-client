import React, { FC } from "react";
import "./SettingSwitch.css";

interface Props {
    onChange: (value: boolean) => void;
    name: string;
    icon: string;
    value: boolean;
    onText?: string;
    offText?: string;
}

const SettingSwtich: FC<Props> = ({
    onChange,
    name,
    icon,
    value,
    onText = "On",
    offText = "Off",
}) => {
    const on = value ? "on" : "off";
    return (
        <div className={`setting-switch ${on}`} onClick={() => onChange(!value)}>
            <div className="setting-name">{name}</div>
            <div className={`icon sprite-${icon}-${on}`}></div>
            <div className="switch">
                <div className="switch-icon sprite-switch"></div>
                <div className="texts">
                    <div className="off-text">{offText}</div>
                    <div className="on-text">{onText}</div>
                </div>
            </div>
        </div>
    );
};

export default SettingSwtich;
