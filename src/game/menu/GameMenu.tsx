import React, { FC, useState, useEffect, useRef } from "react";
import "./GameMenu.css";
import TeamsPanel from "./TeamsPanel";
import SettingsPanel from "./SettingsPanel";

interface Props {}

const GameMenu: FC<Props> = (props) => {
    const [opened, setOpened] = useState(false);
    const panelEl = useRef<HTMLDivElement>(null);
    useEffect(() => {
        const close = () => setOpened(false);
        const dontClose = (e: UIEvent) => {
            e.stopPropagation();
        };
        const panel = panelEl.current;
        document.body.addEventListener("mousedown", close);
        document.body.addEventListener("touchstart", close);
        panel?.addEventListener("mousedown", dontClose);
        panel?.addEventListener("touchstart", dontClose);
        return () => {
            document.body.removeEventListener("mousedown", close);
            document.body.removeEventListener("touchstart", close);
            panel?.removeEventListener("mousedown", dontClose);
            panel?.removeEventListener("touchstart", dontClose);
        };
    }, []);
    return (
        <>
            {!opened ? (
                <button
                    className="menu-open-button button-round neutral"
                    onClick={() => setOpened(true)}
                >
                    <div className="icon sprite-menu-icon"></div>
                </button>
            ) : null}
            <div className={`game-menu ${opened ? "open" : ""}`} ref={panelEl}>
                <SettingsPanel />
                <TeamsPanel />
            </div>
        </>
    );
};

export default GameMenu;
