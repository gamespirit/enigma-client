import React, { FC } from "react";
import "./TeamsPanel.css";
import animals from "../../store/animals";
import { BorderedWindow, SmallTitle } from "../../app/window";

interface Props {}

const TeamsPanel: FC<Props> = (props) => {
    const randomIcon = () => animals[Math.floor(Math.random() * animals.length)].icon;
    const teams = [
        {
            id: "team1",
            points: 1000,
            icon: randomIcon(),
            color: "primary",
            players: [
                {
                    id: "p1",
                    name: "Schwarzenegger",
                    connected: true,
                },
                {
                    id: "p2",
                    name: "And the gang",
                    connected: true,
                },
                {
                    id: "p3",
                    name: "Noooope",
                    connected: false,
                },
            ],
        },
        {
            id: "team2",
            points: 10000,
            icon: randomIcon(),
            color: "team-red",
            players: [
                {
                    id: "p4",
                    name: "Blah",
                    connected: true,
                },
                {
                    id: "p5",
                    name: "Kul and the gang",
                    connected: true,
                },
                {
                    id: "p6",
                    name: "Yesyesyes",
                    connected: true,
                },
            ],
        },
        {
            id: "team3",
            points: 99000,
            icon: randomIcon(),
            color: "team-blue",
            players: [
                {
                    id: "p7",
                    name: "Arnold",
                    connected: true,
                },
                {
                    id: "p8",
                    name: "Kul and the",
                    connected: true,
                },
                {
                    id: "p9",
                    name: "YAX",
                    connected: false,
                },
            ],
        },
    ];
    const myId = "p1";
    return (
        <div className="right teams-panel">
            {teams.map((team) => (
                <div className="team centerer" key={team.id}>
                    <BorderedWindow colorClass={team.color}>
                        <SmallTitle color={team.color}>
                            <span className="team-icon">{team.icon}</span>
                            <span className="team-points">{team.points}</span>
                        </SmallTitle>
                        <div className="players">
                            {team.players.map((player) => (
                                <div
                                    className={`player${player.connected ? "" : " disconnected"}${
                                        myId === player.id ? " me" : ""
                                    }`}
                                    key={player.id}
                                >
                                    <div className="player-content">
                                        <span className="disconnected-icon sprite-disconnected-icon"></span>
                                        <span className="player-name">{player.name}</span>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </BorderedWindow>
                </div>
            ))}
        </div>
    );
};

export default TeamsPanel;
