export const gameScreens = {
    GAME_START_SCREEN: "GAME_START_SCREEN",
    MAIN_GAME_SCREEN: "MAIN_GAME_SCREEN",
    MINI_GAME_SCREEN: "MINI_GAME_SCREEN",
    GAME_OVER_SCREEN: "GAME_OVER_SCREEN",
};

export type GameScreen =
    | typeof gameScreens.GAME_START_SCREEN
    | typeof gameScreens.MAIN_GAME_SCREEN
    | typeof gameScreens.MINI_GAME_SCREEN
    | typeof gameScreens.GAME_OVER_SCREEN;
