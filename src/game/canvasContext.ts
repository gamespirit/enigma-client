import { createContext } from "react";

export interface CanvasContextProps {
    view: { width: number; height: number };
    orientation: Orientation;
    stage?: SVGSVGElement | null;
    bounds: DOMRect;
    ratio: number;
}

export type Orientation = "portrait" | "landscape";

export const defaultView = { width: 420, height: 360, padding: 35 };

export const defaultCanvasContext: CanvasContextProps = {
    view: defaultView,
    orientation: "portrait",
    ratio: 1,
    bounds: document.body.getBoundingClientRect(),
};

export const CanvasContext = createContext<CanvasContextProps>(defaultCanvasContext);
