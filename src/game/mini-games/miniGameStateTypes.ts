import { NOT_LOADED } from "./miniGameTypes";
import { PolyMatchState } from "./poly-match/polyMatchStateTypes";

export interface NotLoadedMiniGameState {
    gameType: typeof NOT_LOADED;
    teamData?: {
        [teamId: string]: any;
    };
}

export type MiniGameState = PolyMatchState | NotLoadedMiniGameState;
