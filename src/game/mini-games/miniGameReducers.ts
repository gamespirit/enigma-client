import { NOT_LOADED, POLY_MATCH } from "./miniGameTypes";
import { polyMatchReducer } from "./poly-match/polyMatchReducer";

export const miniGameReducers = {
    [POLY_MATCH]: polyMatchReducer,
    [NOT_LOADED]: () => {},
};
