export const POLY_MATCH = "POLY_MATCH";
export const NOT_LOADED = "NOT_LOADED";
export type MiniGameType = typeof POLY_MATCH | typeof NOT_LOADED;
