import React, { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../store/reducers";
import * as miniGameTypes from "../miniGameTypes";
import PolyMatch from "../poly-match/components/PolyMatch";

const connector = connect((state: RootState) => ({
    type: state.game.miniGame.gameType,
    teams: state.game.teams!,
}));

export interface MiniGameProps {
    teamId: string;
}

type Props = ConnectedProps<typeof connector>;

const MiniGame: FC<Props> = ({ type, teams }) => {
    return (
        <div className="mini-game centerer">
            {type === miniGameTypes.POLY_MATCH &&
                teams.allIds.map((teamId, i) => <PolyMatch teamId={teamId} key={teamId + i} />)}
        </div>
    );
};

export default connector(MiniGame);
