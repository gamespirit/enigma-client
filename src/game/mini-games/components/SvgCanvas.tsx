import React, { FC, useEffect, useRef, useState } from "react";
import { CanvasContext, defaultView, Orientation } from "../../canvasContext";
import "./SvgCanvas.css";

interface Props {}

const SvgCanvas: FC<Props> = ({ children }) => {
    const container = useRef<SVGSVGElement>(null);
    const [bounds, setBounds] = useState(document.body.getBoundingClientRect());
    const body = document.body.getBoundingClientRect();
    const orientation: Orientation = body.width < body.height ? "portrait" : "landscape";
    useEffect(() => {
        const c = container.current;
        const onResize = () => {
            c && setBounds(c.getBoundingClientRect());
        };
        c && setBounds(c.getBoundingClientRect());
        window.addEventListener("resize", onResize);
        return () => {
            window.removeEventListener("resize", onResize);
        };
    }, [container]);
    const view = defaultView;
    const ratio =
        bounds.width < view.width ? bounds.width / view.width : bounds.height / view.height;
    return (
        <CanvasContext.Provider
            value={{ view, orientation, stage: container.current, ratio, bounds }}
        >
            <div className="svg-canvas-container">
                <div className="centerer">
                    <svg
                        className="svg-canvas fix-ratio"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox={`0 0 ${view.width} ${view.height}`}
                        ref={container}
                    >
                        {children}
                    </svg>
                </div>
            </div>
        </CanvasContext.Provider>
    );
};

export default SvgCanvas;
