import { call, delay, fork, put, select, take } from "redux-saga/effects";
import { listenForSocketEvents } from "../../../generatorHelpers";
import { RootState } from "../../../store/reducers";
import { miniGameEvents } from "../miniGameEvents";
import {
    emitMoveVertex,
    emitReleaseVertex,
    moveVertex,
    reassignVertices,
    updateVertices,
} from "./polyMatchActionCreators";
import * as actions from "./polyMatchActions";
import {
    EmitMoveVertexAction,
    EmitReleaseVertexAction,
    MoveVertexAction,
    ReleaseVertexAction,
} from "./polyMatchActionTypes";
import { polyMatchEvents } from "./polyMatchEvents";
import { PolyMatchState, PolyMatchTeamDataState } from "./polyMatchStateTypes";

export function* polyMatchSaga(socket: SocketIOClient.Socket) {
    yield fork(moveVertexSaga);
    yield fork(releaseVertexSaga);
    yield fork(listenForVertexUpdates, socket);
    yield fork(emitVertexMoveThrottled, socket);
    yield fork(emitReleaseVertexSaga, socket);
    yield fork(listenForReassignment, socket);
}

function* listenForReassignment(socket: SocketIOClient.Socket) {
    yield call(listenForSocketEvents, socket, [
        {
            name: miniGameEvents.MINIGAME_CONTROLS_CHANGED,
            handler: function* (vertexAssignments: number[]) {
                yield put(reassignVertices(vertexAssignments));
            },
        },
    ]);
}

function* emitVertexMoveThrottled(socket: SocketIOClient.Socket) {
    while (true) {
        const action: EmitMoveVertexAction = yield take(actions.PM_EMIT_MOVE_VERTEX);
        socket.emit(polyMatchEvents.MOVE_VERTEX, action.controlIndex, action.position);
        yield delay(20);
    }
}

function* emitReleaseVertexSaga(socket: SocketIOClient.Socket) {
    while (true) {
        const action: EmitReleaseVertexAction = yield take(actions.PM_EMIT_RELEASE_VERTEX);
        socket.emit(
            polyMatchEvents.RELEASE_VERTEX,
            action.controlIndex,
            action.snappedVertexIndex,
            action.position
        );
    }
}

function* releaseVertexSaga() {
    while (true) {
        const action: ReleaseVertexAction = yield take(actions.PM_PLAYER_RELEASED_VERTEX);
        let pos = action.position;
        const state: RootState = yield select();
        const targetVertices = (state.game.miniGame as PolyMatchState).targetVertices;
        let snappedVertexIndex = -1;
        let distX = 20;
        let distY = 20;
        for (let i = 0; i < targetVertices.length; i++) {
            const dx = Math.abs(pos[0] - targetVertices[i][0]);
            const dy = Math.abs(pos[1] - targetVertices[i][1]);
            if (dx < distX && dy < distY) {
                distX = dx;
                distY = dy;
                snappedVertexIndex = i;
            }
        }
        if (snappedVertexIndex > -1) {
            pos = targetVertices[snappedVertexIndex];
            yield put(moveVertex(action.teamId, pos, action.controlIndex));
            yield put(emitReleaseVertex(action.controlIndex, snappedVertexIndex, pos));
        }
    }
}

function* moveVertexSaga() {
    while (true) {
        const action: MoveVertexAction = yield take(actions.PM_PLAYER_MOVED_VERTEX);
        yield put(moveVertex(action.teamId, action.position, action.controlIndex));
        yield put(emitMoveVertex(action.controlIndex, action.position));
    }
}

const myTeamId = (state: RootState) => state.game.teamAssignments?.byUid[state.app.session.uid!];

function* listenForVertexUpdates(socket: SocketIOClient.Socket) {
    yield call(listenForSocketEvents, socket, [
        {
            name: polyMatchEvents.UPDATE_VERTICES,
            handler: function* (teamData: PolyMatchTeamDataState) {
                const teamId: string = yield select(myTeamId);
                yield put(updateVertices(teamData, teamId));
            },
        },
    ]);
}
