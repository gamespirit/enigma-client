import { MINIGAME_CONTROLS_CHANGED } from "../miniGameActions";
import { POLY_MATCH } from "../miniGameTypes";
import * as actions from "./polyMatchActions";
import { PolyMatchTeamDataState } from "./polyMatchStateTypes";

export interface MoveVertexAction {
    type: typeof actions.PM_PLAYER_MOVED_VERTEX;
    miniGameType: typeof POLY_MATCH;
    teamId: string;
    controlIndex: number;
    position: [number, number];
}

export interface ReleaseVertexAction {
    type: typeof actions.PM_PLAYER_RELEASED_VERTEX;
    miniGameType: typeof POLY_MATCH;
    teamId: string;
    controlIndex: number;
    position: [number, number];
}

export interface UpdateVerticesAction {
    type: typeof actions.PM_UPDATE_VERTICES;
    miniGameType: typeof POLY_MATCH;
    teamData: PolyMatchTeamDataState;
    teamId: string;
}

export interface EmitMoveVertexAction {
    type: typeof actions.PM_EMIT_MOVE_VERTEX;
    miniGameType: typeof POLY_MATCH;
    controlIndex: number;
    position: [number, number];
}

export interface EmitReleaseVertexAction {
    type: typeof actions.PM_EMIT_RELEASE_VERTEX;
    miniGameType: typeof POLY_MATCH;
    controlIndex: number;
    snappedVertexIndex: number;
    position: [number, number];
}

export interface ReassignVerticesAction {
    type: typeof MINIGAME_CONTROLS_CHANGED;
    miniGameType: typeof POLY_MATCH;
    vertexAssignments: number[];
}

export type PolyMatchAction =
    | MoveVertexAction
    | ReleaseVertexAction
    | UpdateVerticesAction
    | EmitMoveVertexAction
    | EmitReleaseVertexAction
    | ReassignVerticesAction;
