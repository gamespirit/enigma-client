import { POLY_MATCH } from "../miniGameTypes";

export interface PolyMatchTeamDataState {
    [teamId: string]: [number, number][];
}

export interface PolyMatchState {
    gameType: typeof POLY_MATCH;
    targetVertices: [number, number][];
    vertexAssignments: number[]; // index = control index. value = vertex index
    edges: [number, number][]; // edges = [source vertex, target vertex][]
    teamData: PolyMatchTeamDataState;
}
