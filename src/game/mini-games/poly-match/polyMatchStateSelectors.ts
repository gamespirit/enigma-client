import { RootState } from "../../../store/reducers";
import { PolyMatchState } from "./polyMatchStateTypes";

export const getVertexAssignments = (state: RootState, teamId: string): number[] =>
    (state.game.miniGame as PolyMatchState).vertexAssignments;
