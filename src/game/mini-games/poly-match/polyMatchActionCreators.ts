import { PolyMatchAction, ReassignVerticesAction } from "./polyMatchActionTypes";
import * as actions from "./polyMatchActions";
import { PolyMatchTeamDataState } from "./polyMatchStateTypes";
import { POLY_MATCH } from "../miniGameTypes";
import { MINIGAME_CONTROLS_CHANGED } from "../miniGameActions";

export const moveVertex = (
    teamId: string,
    position: [number, number],
    controlIndex: number
): PolyMatchAction => ({
    type: actions.PM_PLAYER_MOVED_VERTEX,
    miniGameType: POLY_MATCH,
    teamId,
    position,
    controlIndex,
});

export const releaseVertex = (
    teamId: string,
    position: [number, number],
    controlIndex: number
): PolyMatchAction => ({
    type: actions.PM_PLAYER_RELEASED_VERTEX,
    miniGameType: POLY_MATCH,
    teamId,
    position,
    controlIndex,
});

export const updateVertices = (data: PolyMatchTeamDataState, teamId: string): PolyMatchAction => ({
    type: actions.PM_UPDATE_VERTICES,
    teamData: data,
    miniGameType: POLY_MATCH,
    teamId,
});

export const emitMoveVertex = (
    controlIndex: number,
    position: [number, number]
): PolyMatchAction => ({
    type: actions.PM_EMIT_MOVE_VERTEX,
    miniGameType: POLY_MATCH,
    position,
    controlIndex,
});

export const emitReleaseVertex = (
    controlIndex: number,
    snappedVertexIndex: number,
    position: [number, number]
): PolyMatchAction => ({
    type: actions.PM_EMIT_RELEASE_VERTEX,
    miniGameType: POLY_MATCH,
    position,
    controlIndex,
    snappedVertexIndex,
});

export const reassignVertices = (vertexAssignments: number[]): ReassignVerticesAction => ({
    type: MINIGAME_CONTROLS_CHANGED,
    miniGameType: POLY_MATCH,
    vertexAssignments,
});
