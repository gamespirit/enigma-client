import { Draft } from "immer";
import { MINIGAME_CONTROLS_CHANGED } from "../miniGameActions";
import { PM_PLAYER_MOVED_VERTEX, PM_UPDATE_VERTICES } from "./polyMatchActions";
import { PolyMatchAction } from "./polyMatchActionTypes";
import { PolyMatchState } from "./polyMatchStateTypes";

export const polyMatchReducer = (state: Draft<PolyMatchState>, action: PolyMatchAction) => {
    switch (action.type) {
        case PM_PLAYER_MOVED_VERTEX: {
            state.teamData[action.teamId][state.vertexAssignments[action.controlIndex]] =
                action.position;
            break;
        }
        case PM_UPDATE_VERTICES: {
            const myTeamId = action.teamId;
            const teamData = action.teamData;
            teamData[myTeamId] = teamData[myTeamId].map((v, i) =>
                state.vertexAssignments?.includes(i) ? state.teamData[myTeamId][i] : v
            );
            state.teamData = teamData;
            break;
        }
        case MINIGAME_CONTROLS_CHANGED: {
            state.vertexAssignments = action.vertexAssignments;
            break;
        }
    }
};
