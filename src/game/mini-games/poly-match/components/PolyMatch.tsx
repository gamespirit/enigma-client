import React, { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../../store/reducers";
import { MiniGameProps } from "../../components/MiniGame";
import SvgCanvas from "../../components/SvgCanvas";
import Control from "../../utils/control/Control";
import { moveVertex, releaseVertex } from "../polyMatchActionCreators";
import { PolyMatchState } from "../polyMatchStateTypes";
import "./PolyMatch.css";

const connector = connect(
    (state: RootState, ownProps: MiniGameProps) => ({
        game: state.game.miniGame as PolyMatchState,
        uid: state.app.session.uid!,
        teamId: ownProps.teamId,
    }),
    { moveVertex, releaseVertex }
);
type Props = ConnectedProps<typeof connector>;

const PolyMatch: FC<Props> = ({ game, uid, teamId, moveVertex, releaseVertex }) => {
    const targetVertices: [number, number][] = game.targetVertices;
    const currentVertices = game.teamData[teamId];
    const edges = game.edges;

    const handleMoveVertex = (pos: [number, number], i: number) => {
        moveVertex(teamId, pos, i);
    };

    const handleReleaseVertex = (pos: [number, number], i: number) => {
        releaseVertex(teamId, pos, i);
    };

    return (
        <SvgCanvas>
            <g className="target-shape">
                {edges.map((edge, i) => (
                    <line
                        key={i}
                        x1={targetVertices[edge[0]][0]}
                        y1={targetVertices[edge[0]][1]}
                        x2={targetVertices[edge[1]][0]}
                        y2={targetVertices[edge[1]][1]}
                    />
                ))}
            </g>
            <g className="target-points">
                {targetVertices.map((vertex, i) => (
                    <circle key={i} cx={vertex[0]} cy={vertex[1]} r="3" />
                ))}
            </g>
            <g className="control-shape">
                {edges.map((edge, i) => (
                    <line
                        key={i}
                        x1={currentVertices[edge[0]][0]}
                        y1={currentVertices[edge[0]][1]}
                        x2={currentVertices[edge[1]][0]}
                        y2={currentVertices[edge[1]][1]}
                    />
                ))}
            </g>
            <g className="control-points">
                {currentVertices.map((vertex, i) => (
                    <circle key={i} cx={vertex[0]} cy={vertex[1]} r="3" />
                ))}
            </g>
            {game.vertexAssignments &&
                game.vertexAssignments.map((vertexIndex, controlIndex) => (
                    <Control
                        key={controlIndex}
                        x={currentVertices[vertexIndex][0]}
                        y={currentVertices[vertexIndex][1]}
                        interactive={true}
                        color="primary"
                        onMove={(pos) => handleMoveVertex(pos, controlIndex)}
                        onRelease={(pos) => handleReleaseVertex(pos, controlIndex)}
                    />
                ))}
        </SvgCanvas>
    );
};

export default connector(PolyMatch);
