import { RootState } from "../../store/reducers";

export const miniGameTeamData = (state: RootState) => state.game.miniGame.teamData;
