import { take } from "redux-saga/effects";
import { NOT_LOADED, POLY_MATCH } from "./miniGameTypes";
import { polyMatchSaga } from "./poly-match/polyMatchSaga";

function* notLoadedSaga(socket: SocketIOClient.Socket) {
    yield take("WAIT_INDEFINITELY");
}

export const miniGameSagas = {
    [POLY_MATCH]: polyMatchSaga,
    [NOT_LOADED]: notLoadedSaga,
};
