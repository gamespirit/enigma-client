import { PolyMatchAction } from "./poly-match/polyMatchActionTypes";

export type MiniGameAction = PolyMatchAction;
