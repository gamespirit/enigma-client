import React, { FC, useCallback, useContext, useEffect, useRef, useState } from "react";
import { CanvasContext, defaultView } from "../../../canvasContext";
import "./Control.css";

interface Props {
    x: number;
    y: number;
    color: string;
    interactive: boolean;
    onMove: (pos: [number, number]) => void;
    onRelease: (pos: [number, number]) => void;
}

function clamp(num: number, min: number, max: number) {
    return Math.min(Math.max(num, min), max);
}

const Control: FC<Props> = ({ x, y, color, interactive, onMove, onRelease }) => {
    const offset = 50;
    const control = useRef<SVGCircleElement>(null);
    const [drag, setDrag] = useState(false);
    const { ratio, bounds } = useContext(CanvasContext);
    const padding = defaultView.padding * ratio;

    const getOffset = useCallback(
        (x: number, y: number): [number, number] => [
            (clamp(x, bounds.left + padding, bounds.right - padding) - bounds.x) / ratio,
            (clamp(y, bounds.top + padding, bounds.bottom - padding) - bounds.y) / ratio,
        ],
        [bounds, ratio, padding]
    );

    const handleRelease = useCallback(
        (x: number, y: number) => {
            setDrag(false);
            onRelease(getOffset(x, y));
        },
        [onRelease, setDrag, getOffset]
    );

    const handleMove = useCallback(
        (x: number, y: number) => {
            onMove(getOffset(x, y));
        },
        [onMove, getOffset]
    );

    useEffect(() => {
        const onMouseMove = (e: MouseEvent) => handleMove(e.clientX, e.clientY);
        const onTouchMove = (e: TouchEvent) =>
            handleMove(e.touches[0].clientX, e.touches[0].clientY);
        const stop = (e: TouchEvent | MouseEvent) => {
            if (e instanceof TouchEvent) {
                handleRelease(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
            } else {
                handleRelease(e.clientX, e.clientY);
            }
        };
        const start = (e: TouchEvent | MouseEvent) => {
            if (interactive && e.target === control.current) {
                setDrag(true);
            }
        };
        document.body.addEventListener("mousedown", start);
        document.body.addEventListener("touchstart", start);
        if (drag) {
            document.body.addEventListener("mousemove", onMouseMove);
            document.body.addEventListener("touchmove", onTouchMove);
            document.body.addEventListener("mouseup", stop);
            document.body.addEventListener("touchend", stop);
            document.body.addEventListener("touchcancel", stop);
        }
        return () => {
            document.body.removeEventListener("mousedown", start);
            document.body.removeEventListener("touchstart", start);
            document.body.removeEventListener("mousemove", onMouseMove);
            document.body.removeEventListener("touchmove", onTouchMove);
            document.body.removeEventListener("mouseup", stop);
            document.body.removeEventListener("touchend", stop);
            document.body.removeEventListener("touchcancel", stop);
        };
    }, [drag, interactive, handleMove, handleRelease]);

    return (
        <svg x={x - offset} y={y - offset}>
            <g className={`control-handle ${color} ${!interactive ? "inactive" : ""}`}>
                <circle cx={offset} cy={offset} r="5" className="control-inner" />
                {interactive && <circle cx={offset} cy={offset} className="control-outer" />}
                {interactive && (
                    <circle cx={offset} cy={offset} className="control-detector" ref={control} />
                )}
            </g>
        </svg>
    );
};

export default Control;
