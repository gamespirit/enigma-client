import React, { FC } from "react";
import "./MainGameScreen.css";

interface Props {}

const MainGameScreen: FC<Props> = ({ children }) => {
    return <div className="main-game-screen">{children}</div>;
};

export default MainGameScreen;
