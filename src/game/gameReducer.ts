import produce, { Draft } from "immer";
import * as actions from "./gameActions";
import { GameActionTypes } from "./gameActionTypes";
import { gameScreens } from "./gameScreens";
import { GameState } from "./gameStateTypes";
import { MiniGameAction } from "./mini-games/miniGameActionTypes";
import { miniGameReducers } from "./mini-games/miniGameReducers";
import * as miniGameTypes from "./mini-games/miniGameTypes";

const initialState: GameState = {
    screen: gameScreens.GAME_START_SCREEN,
    miniGame: {
        gameType: miniGameTypes.NOT_LOADED,
    },
};

export const gameReducer = produce((draft: Draft<GameState>, action: GameActionTypes) => {
    switch (action.type) {
        case actions.JOIN_GAME: {
            draft.gameId = action.gameId;
            break;
        }
        case actions.LEAVE_GAME: {
            delete draft.gameId;
            draft.screen = gameScreens.GAME_START_SCREEN;
            draft.miniGame = {
                gameType: miniGameTypes.NOT_LOADED,
            };
            break;
        }
        case actions.SHOW_SCREEN: {
            draft.screen = action.screen;
            break;
        }
        case actions.UPDATE_GAME_STATE: {
            Object.assign(draft, action.gameState);
            break;
        }
        case actions.UPDATE_MINI_GAME_STATE: {
            Object.assign(draft.miniGame, action.miniGameState);
            break;
        }
        case actions.START_MINI_GAME: {
            draft.miniGame = action.miniGameState;
            draft.screen = gameScreens.MINI_GAME_SCREEN;
            break;
        }
        case actions.STOP_MINI_GAME: {
            draft.miniGame = {
                gameType: miniGameTypes.NOT_LOADED,
            };
            break;
        }
        default: {
            if (((action as any) as MiniGameAction).miniGameType) {
                miniGameReducers[((action as any) as MiniGameAction).miniGameType](
                    draft.miniGame as any,
                    action as any
                );
            }
        }
    }
}, initialState);
