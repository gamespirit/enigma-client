import * as actions from "./gameActions";
import { GameActionTypes } from "./gameActionTypes";
import { GameScreen } from "./gameScreens";
import { GameState } from "./gameStateTypes";
import { MiniGameState } from "./mini-games/miniGameStateTypes";

export const joinGame = (gameId: string): GameActionTypes => ({
    type: actions.JOIN_GAME,
    gameId,
});

export const leaveGame = (): GameActionTypes => ({
    type: actions.LEAVE_GAME,
});

export const updateGameData = (data: GameState): GameActionTypes => ({
    type: actions.UPDATE_GAME_STATE,
    gameState: data,
});

export const joinGameRejected = (reason: string): GameActionTypes => ({
    type: actions.JOIN_GAME_REJECTED,
    reason,
});

export const showScreen = (screen: GameScreen): GameActionTypes => ({
    type: actions.SHOW_SCREEN,
    screen,
});

export const startMiniGame = (data: MiniGameState): GameActionTypes => ({
    type: actions.START_MINI_GAME,
    miniGameState: data,
});

export const stopMiniGame = (): GameActionTypes => ({
    type: actions.STOP_MINI_GAME,
});

export const updateMiniGame = (data: Partial<MiniGameState>): GameActionTypes => ({
    type: actions.UPDATE_MINI_GAME_STATE,
    miniGameState: data,
});
