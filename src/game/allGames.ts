import { ReactElement } from "react";
import { HANGMAN, hangmanSymbolResolver } from "./main-games";
import * as miniGameTypes from "./mini-games/miniGameTypes";

export const MINI_GAME = "MINI_GAME";
export const MAIN_GAME = "MAIN_GAME";

export type AllGames = {
    [game: string]: {
        type: typeof MINI_GAME | typeof MAIN_GAME;
        name: string;
    };
};

export const games: AllGames = {
    [miniGameTypes.POLY_MATCH]: { type: MINI_GAME, name: "Poly-Match" },
    [HANGMAN]: { type: MAIN_GAME, name: "Hangman" },
};

export interface HistorySymbolResolvers {
    [game: string]: (symbols: any, color: string) => ReactElement | null;
}

export const historySymbolResolvers: HistorySymbolResolvers = {
    [HANGMAN]: hangmanSymbolResolver,
};
