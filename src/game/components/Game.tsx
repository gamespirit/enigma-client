import React, { FC, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { RootState } from "../../store/reducers";
import { joinGame } from "../gameActionCreators";
import { gameScreens } from "../gameScreens";
import MainGameScreen from "../main-game-screen/MainGameScreen";
// import GameOverScreen from "../game-over-screen/GameOverScreen";
// import RoundStartScreen from "./round-start-screen/RoundStartScreen";
import Hangman from "../main-games/hangman/components/Hangman";
// import MainGameScreen from "../main-game-screen/MainGameScreen";
// import TurnInfo from "../turn-info/TurnInfo";
// import TeamIcons from "../team-icons/TeamIcons";
// import Timer from "../timer/Timer";
import GameMenu from "../menu/GameMenu";
import MiniGame from "../mini-games/components/MiniGame";
import "./Game.css";

type RouteProps = RouteComponentProps<{ gameId: string }>;

const connector = connect(
    (state: RootState, ownProps: RouteProps) => ({
        gameId: ownProps.match.params.gameId,
        screen: state.game.screen,
        miniGameState: state.game.miniGame,
        teams: state.game.teams,
    }),
    {
        joinGame,
    }
);

type Props = ConnectedProps<typeof connector>;

const Game: FC<Props> = ({ gameId, screen, miniGameState, joinGame }) => {
    useEffect(() => {
        joinGame(gameId);
        return () => {
            // cleanup
        };
    }, [gameId, joinGame]);
    return (
        <div className="game-container centerer">
            <div className="game-area">
                {screen === gameScreens.GAME_START_SCREEN && "Starting Game.."}
                {screen === gameScreens.MAIN_GAME_SCREEN && (
                    <MainGameScreen>
                        <Hangman />
                    </MainGameScreen>
                )}
                {/* <RoundStartScreen>Round 1</RoundStartScreen> */}
                {/* <TurnInfo /> */}
                {/* <TeamIcons /> */}
                {/* {props.match.params.gameId} */}
                {/* <Timer /> */}
                {/* <GameOverScreen /> */}
                {screen === gameScreens.MINI_GAME_SCREEN && <MiniGame />}
                <GameMenu />
            </div>
        </div>
    );
};

export default withRouter(connector(Game));
