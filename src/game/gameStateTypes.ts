import { GameScreen } from "./gameScreens";
import { MiniGameState } from "./mini-games/miniGameStateTypes";

export interface GameState {
    miniGame: MiniGameState;
    gameId?: string;
    teamAssignments?: TeamAssignments;
    teams?: GameTeamCollection;
    screen: GameScreen;
    players?: {
        allowedUids: string[]; // uids
        connectedUids: string[]; // uids
    };
}

export interface TeamAssignments {
    byUid: {
        [uid: string]: string; // uid -> teamId
    };
    byTeam: {
        [teamId: string]: string[]; // teamId -> uids
    };
}

export interface GameTeamCollection {
    byId: {
        [teamId: string]: GameTeam;
    };
    allIds: string[];
}

export interface GameTeam {
    displayId: number;
    players: string[]; // uids
    points: number;
}
