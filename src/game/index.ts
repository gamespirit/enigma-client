import { gameReducer } from "./gameReducer";
import "../services/gsap";

export { default as Game } from "./components/Game";
export { gameReducer };
