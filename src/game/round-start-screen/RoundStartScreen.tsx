import React, { FC } from "react";
import "./RoundStartScreen.css";

interface Props {}

const RoundStartScreen: FC<Props> = ({ children }) => {
    return <div className="round-start-screen">{children}</div>;
};

export default RoundStartScreen;
