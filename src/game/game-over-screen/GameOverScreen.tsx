import React, { FC, useState } from "react";
import history from "../../services/history";
import animals from "../../store/animals";
import "./GameOverScreen.css";
import Podium from "./Podium";
import RoundHistory from "./round-history/RoundHistory";

interface Props {}

const GameOverScreen: FC<Props> = (props) => {
    const [showHistory, setShowHistory] = useState(false);
    const title = showHistory ? "Round history" : "Game over!";
    const returnToLobby = () => {
        history.push("/lobby");
    };
    const randomIcon = () => animals[Math.floor(Math.random() * animals.length)].icon;
    const teams = [
        {
            id: "t1",
            icon: randomIcon(),
            color: "team-red",
            points: 35500,
            players: [
                {
                    id: "p7",
                    name: "Arnold Schwarzenegger",
                },
                {
                    id: "p8",
                    name: "Raj Kutrapali",
                },
                {
                    id: "p9",
                    name: "Fa koffer ratatuil",
                },
            ],
        },
        {
            id: "t2",
            icon: randomIcon(),
            color: "team-blue",
            points: 1500,
            players: [
                {
                    id: "p1",
                    name: "Arnold Schwarzenegger",
                },
                {
                    id: "p2",
                    name: "Raj Kutrapali",
                },
                {
                    id: "p3",
                    name: "Fa koffer ratatuil",
                },
            ],
        },
        {
            id: "t3",
            icon: randomIcon(),
            color: "primary",
            points: 500,
            players: [
                {
                    id: "p4",
                    name: "Arnold Schwarzenegger",
                },
                {
                    id: "p5",
                    name: "Raj Kutrapali",
                },
                {
                    id: "p6",
                    name: "Fa koffer ratatuil",
                },
            ],
        },
    ];
    return (
        <>
            <div className={`game-over-screen`}>
                <div className="title">{title}</div>
                {showHistory ? (
                    <RoundHistory />
                ) : (
                    <Podium teams={teams} onShowDetails={() => setShowHistory(true)} />
                )}
            </div>
            <button
                className="button-round neutral return-to-lobby-button"
                onClick={() => returnToLobby()}
            >
                <div className="icon sprite-exit-icon"></div>
            </button>
        </>
    );
};

export default GameOverScreen;
