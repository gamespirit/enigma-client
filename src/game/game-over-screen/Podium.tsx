import React, { FC, useEffect, useState } from "react";
import "./Podium.css";

interface Props {
    onShowDetails: () => void;
    teams: {
        id: string;
        icon: string;
        color: string;
        points: number;
        players: {
            id: string;
            name: string;
        }[];
    }[];
}

const Podium: FC<Props> = ({ teams, onShowDetails }) => {
    const [hidden, setHidden] = useState(true);
    useEffect(() => {
        const timeout = window.setTimeout(() => setHidden(false), 500);
        return () => {
            clearTimeout(timeout);
        };
    });
    return (
        <div className={`podium size-${teams.length} ${hidden ? "hidden" : ""}`}>
            {teams.map((team, i) => (
                <div className={`place-container place-${i + 1}`} key={team.id}>
                    <div className={`place ${team.color}`}>
                        <div className="place-top"></div>
                        <div className="team-icon">{team.icon}</div>
                        <div className="team-points">{team.points}</div>
                        <div className="players">
                            {team.players.map((player) => (
                                <div className="player" key={player.id}>
                                    {player.name}
                                </div>
                            ))}
                        </div>
                        {i === 0 ? (
                            <div className="details-button-container">
                                <button
                                    className="button-round details-button"
                                    onClick={onShowDetails}
                                >
                                    <div className={`icon sprite-show-details-${team.color}`}></div>
                                </button>
                                <div className="button-text">Details</div>
                            </div>
                        ) : null}
                    </div>
                </div>
            ))}
        </div>
    );
};

export default Podium;
