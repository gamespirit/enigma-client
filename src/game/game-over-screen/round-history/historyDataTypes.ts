import { HangmanRound } from "../../main-games";
import { MiniGameType } from "../../mini-games/miniGameTypes";

export const CUP = "CUP";
export const ROUND_POINTS = "ROUND_POINTS";
export const MINI_GAME_ROUND = "MINI_GAME_ROUND";
export const MAIN_GAME_ROUND = "MAIN_GAME_ROUND";

export interface CupSymbol {
    type: typeof CUP;
}

export interface PointsSymbol {
    type: typeof ROUND_POINTS;
    value: number;
}

export interface RoundHistory {
    teams: HistoryTeams;
    gameRoundsById: {
        [gameRoundId: string]: {
            miniRoundsById: {
                [miniRoundId: string]: RoundData;
            };
            allMiniRoundIds: string[];
        };
    };
    allGameRoundIds: string[];
}

export interface HistoryTeams {
    byId: {
        [teamId: string]: {
            name: string;
            color: string;
        };
    };
    allIds: string[];
}

export interface MiniGameRound {
    roundType: typeof MINI_GAME_ROUND;
    game: MiniGameType;
    dataByTeamId: {
        [teamId: string]: {
            data: MiniGameRoundData;
        };
    };
}

export interface MiniGameRoundData {
    time: number;
    actions: number;
    points: number;
}

export interface MainGameRound<TMainGame, TMainGameSymbols> {
    roundType: typeof MAIN_GAME_ROUND;
    game: TMainGame;
    dataByTeamId: {
        [teamId: string]: {
            symbols: MainGameSymbolsTypes<TMainGameSymbols>[];
        };
    };
}

export type MainGameRoundTypes = HangmanRound;
export type MainGameSymbolsTypes<TMainGameSymbols> = PointsSymbol | CupSymbol | TMainGameSymbols;
export type RoundData = MiniGameRound | MainGameRoundTypes;
