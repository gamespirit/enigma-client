import React, { ReactElement } from "react";
import { historySymbolResolvers } from "../../allGames";
import { MainGameType } from "../../main-games";
import { CUP, MainGameSymbolsTypes, ROUND_POINTS } from "./historyDataTypes";
import { v4 as uuid } from "uuid";

export function mainGameRound(
    symbols: MainGameSymbolsTypes<any>[],
    color: string,
    game: MainGameType
): ReactElement | null {
    return (
        <td className={color} key={uuid()}>
            {symbols.map((symbol) => {
                switch (symbol.type) {
                    case CUP:
                        return (
                            <React.Fragment key={uuid()}>
                                <span className="separator">|</span>
                                <span className={`icon sprite-cup-${color}`}></span>
                            </React.Fragment>
                        );
                    case ROUND_POINTS:
                        return (
                            <React.Fragment key={uuid()}>
                                <span className="separator">|</span>
                                <span className="points">+{symbol.value}</span>
                            </React.Fragment>
                        );
                    default:
                        return historySymbolResolvers[game](symbol, color);
                }
            })}
        </td>
    );
}
