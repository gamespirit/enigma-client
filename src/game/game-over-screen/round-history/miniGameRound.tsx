import React, { ReactElement } from "react";
import { MiniGameRoundData } from "./historyDataTypes";
import { v4 as uuid } from "uuid";

export function miniGameRound(roundData: MiniGameRoundData, color: string): ReactElement | null {
    return (
        <td className={color} key={uuid()}>
            <span className="time">{roundData.time}s</span>
            <span className="separator">|</span>
            {new Array(roundData.actions).fill(undefined).map((_) => (
                <span className={`icon sprite-star-${color}`} key={uuid()}></span>
            ))}
            <span className="separator">|</span>
            <span className="points">+{roundData.points}</span>
        </td>
    );
}
