import React, { FC } from "react";
import { miniRound } from "./miniRound";
import "./RoundHistory.css";
import { roundHistoryMockData } from "./roundHistoryMockData";
import { v4 as uuid } from "uuid";

interface Props {}

const RoundHistory: FC<Props> = (props) => {
    const data = roundHistoryMockData;
    return (
        <div className="round-history">
            <div className="scroll-window">
                <table className="round-history-table">
                    <thead>
                        <tr className="team-names-header">
                            <th></th>
                            {data.teams.allIds.map((teamId) => {
                                const team = data.teams.byId[teamId];
                                return (
                                    <th key={teamId} className={team.color}>
                                        {team.name}
                                    </th>
                                );
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {data.allGameRoundIds.map((gameRoundId, i) => {
                            const gameRound = data.gameRoundsById[gameRoundId];
                            return (
                                <React.Fragment key={uuid()}>
                                    <tr className="game-round-header" key={uuid()}>
                                        <td colSpan={data.teams.allIds.length + 1}>
                                            Round {i + 1}
                                        </td>
                                    </tr>
                                    {gameRound.allMiniRoundIds.map((miniRoundId, i) => {
                                        const miniRoundData = gameRound.miniRoundsById[miniRoundId];
                                        return miniRound(i + 1, data.teams, miniRoundData);
                                    })}
                                </React.Fragment>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default RoundHistory;
