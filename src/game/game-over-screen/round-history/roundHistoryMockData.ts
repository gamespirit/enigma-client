import { HANGMAN, LETTER_GUESSED, LETTER_USED_BY_OTHER_TEAM } from "../../main-games";
import { POLY_MATCH } from "../../mini-games/miniGameTypes";
import {
    CUP,
    MAIN_GAME_ROUND,
    MINI_GAME_ROUND,
    RoundHistory,
    ROUND_POINTS,
} from "./historyDataTypes";

export const roundHistoryMockData: RoundHistory = {
    gameRoundsById: {
        g1: {
            allMiniRoundIds: ["r1", "r2", "r3", "r4"],
            miniRoundsById: {
                r1: {
                    roundType: MINI_GAME_ROUND,
                    game: POLY_MATCH,
                    dataByTeamId: {
                        t1: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t2: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t3: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                    },
                },
                r2: {
                    roundType: MAIN_GAME_ROUND,
                    game: HANGMAN,
                    dataByTeamId: {
                        t1: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t2: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t3: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                    },
                },
                r3: {
                    roundType: MINI_GAME_ROUND,
                    game: POLY_MATCH,
                    dataByTeamId: {
                        t1: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t2: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t3: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                    },
                },
                r4: {
                    roundType: MAIN_GAME_ROUND,
                    game: HANGMAN,
                    dataByTeamId: {
                        t1: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t2: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t3: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                                { type: CUP },
                            ],
                        },
                    },
                },
            },
        },
        g2: {
            allMiniRoundIds: ["r5", "r6", "r7", "r8"],
            miniRoundsById: {
                r5: {
                    roundType: MINI_GAME_ROUND,
                    game: POLY_MATCH,
                    dataByTeamId: {
                        t1: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t2: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t3: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                    },
                },
                r6: {
                    roundType: MAIN_GAME_ROUND,
                    game: HANGMAN,
                    dataByTeamId: {
                        t1: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t2: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t3: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                    },
                },
                r7: {
                    roundType: MINI_GAME_ROUND,
                    game: POLY_MATCH,
                    dataByTeamId: {
                        t1: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t2: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t3: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                    },
                },
                r8: {
                    roundType: MAIN_GAME_ROUND,
                    game: HANGMAN,
                    dataByTeamId: {
                        t1: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                                { type: CUP },
                            ],
                        },
                        t2: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t3: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                    },
                },
            },
        },
        g3: {
            allMiniRoundIds: ["r9", "r10", "r11", "r12"],
            miniRoundsById: {
                r9: {
                    roundType: MINI_GAME_ROUND,
                    game: POLY_MATCH,
                    dataByTeamId: {
                        t1: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t2: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t3: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                    },
                },
                r10: {
                    roundType: MAIN_GAME_ROUND,
                    game: HANGMAN,
                    dataByTeamId: {
                        t1: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t2: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t3: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                    },
                },
                r11: {
                    roundType: MINI_GAME_ROUND,
                    game: POLY_MATCH,
                    dataByTeamId: {
                        t1: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t2: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                        t3: {
                            data: {
                                actions: 3,
                                time: 10.5,
                                points: 2100,
                            },
                        },
                    },
                },
                r12: {
                    roundType: MAIN_GAME_ROUND,
                    game: HANGMAN,
                    dataByTeamId: {
                        t1: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t2: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: CUP },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                        t3: {
                            symbols: [
                                { type: LETTER_GUESSED, letter: "t", count: 3 },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "r" },
                                { type: LETTER_USED_BY_OTHER_TEAM, letter: "q" },
                                { type: ROUND_POINTS, value: 1200 },
                            ],
                        },
                    },
                },
            },
        },
    },
    allGameRoundIds: ["g1", "g2", "g3"],
    teams: {
        allIds: ["t1", "t2", "t3"],
        byId: {
            t1: {
                color: "team-red",
                name: "Kingwin",
            },
            t2: {
                color: "team-blue",
                name: "Jaglion",
            },
            t3: {
                color: "primary",
                name: "Zonkey",
            },
        },
    },
};
