import React, { ReactElement } from "react";
import { mainGameRound } from "./mainGameRound";
import { miniGameRound } from "./miniGameRound";
import { HistoryTeams, MAIN_GAME_ROUND, MINI_GAME_ROUND, RoundData } from "./historyDataTypes";
import { games } from "../../allGames";

export function miniRound(
    round: number,
    teams: HistoryTeams,
    miniRoundData: RoundData
): ReactElement | null {
    return (
        <tr className="round-data-row" key={round}>
            <td className="game-name">
                {round}. {games[miniRoundData.game].name}
            </td>
            {teams.allIds.map((teamId) => {
                switch (miniRoundData.roundType) {
                    case MINI_GAME_ROUND: {
                        const roundData = miniRoundData.dataByTeamId[teamId].data;
                        const color = teams.byId[teamId].color;
                        return miniGameRound(roundData, color);
                    }
                    case MAIN_GAME_ROUND: {
                        const symbols = miniRoundData.dataByTeamId[teamId].symbols;
                        const color = teams.byId[teamId].color;
                        return mainGameRound(symbols, color, miniRoundData.game);
                    }
                    default:
                        return null;
                }
            })}
        </tr>
    );
}
