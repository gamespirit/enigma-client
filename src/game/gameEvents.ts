// socket events
export const JOIN_GAME = "GAME/JOIN";
export const LEAVE_GAME = "GAME/LEAVE";
export const JOIN_SUCCESS = "GAME/JOIN_SUCCESS";
export const JOIN_REJECTED = "GAME/JOIN_REJECTED";
export const SHOW_SCREEN = "GAME/SHOW_SCREEN";
export const START_MINIGAME = "GAME/MINIGAME_START";
