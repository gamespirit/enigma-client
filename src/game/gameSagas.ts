import { eventChannel, EventChannel, Task } from "redux-saga";
import { call, cancel, fork, put, race, select, take } from "redux-saga/effects";
import history from "../services/history";
import { RootState } from "../store/reducers";
import {
    joinGameRejected,
    showScreen,
    startMiniGame,
    stopMiniGame,
    updateGameData,
} from "./gameActionCreators";
import * as actions from "./gameActions";
import {
    GameActionTypes,
    JoinGameAction,
    JoinGameRejectedAction,
    StartMiniGameAction,
    UpdateGameDataAction,
} from "./gameActionTypes";
import * as events from "./gameEvents";
import { GameScreen } from "./gameScreens";
import { GameState } from "./gameStateTypes";
import { miniGameSagas } from "./mini-games/miniGameSagas";
import { MiniGameState } from "./mini-games/miniGameStateTypes";
import { MiniGameType } from "./mini-games/miniGameTypes";

export function* gameFlow(socket: SocketIOClient.Socket) {
    while (true) {
        const joinAction: JoinGameAction = yield take(actions.JOIN_GAME);
        const joinSuccess: boolean = yield call(joinGame, socket, joinAction.gameId);
        if (joinSuccess) {
            // handle reconnects by starting up the appropriate saga for the visible screen
            const reconnectStateHandlerTask: Task = yield fork(handleReconnectState, socket);

            // start stuff according to game events, cancel them on leaving the game / disconnect
            yield race({
                listenForGameEvents: call(listenForGameEvents, socket),
                miniGames: call(listenForStartMiniGameEvents, socket),
                leaveGame: take(actions.LEAVE_GAME),
            });

            // notify server about leave
            socket.emit(events.LEAVE_GAME);
            // cancel reconnect handler when leaving the game
            yield cancel(reconnectStateHandlerTask);
        } else {
            history.push("/lobby");
        }
    }
}

/** Handle reconnect by starting the appropriate saga for the visible screen. (mini game, main game) */
function* handleReconnectState(socket: SocketIOClient.Socket) {
    const state: GameState = yield select((state: RootState) => state.game);
    if (state.miniGame) {
        yield call(startMiniGameSaga, socket, state.miniGame.gameType);
    }
}

function* listenForStartMiniGameEvents(socket: SocketIOClient.Socket) {
    while (true) {
        const action: StartMiniGameAction = yield take(actions.START_MINI_GAME);
        yield call(startMiniGameSaga, socket, action.miniGameState.gameType);
    }
}

function* startMiniGameSaga(socket: SocketIOClient.Socket, type: MiniGameType) {
    try {
        yield call(miniGameSagas[type], socket);
    } finally {
        yield put(stopMiniGame());
    }
}

function* listenForGameEvents(socket: SocketIOClient.Socket) {
    const channel: EventChannel<GameActionTypes> = yield call(() =>
        eventChannel((emit) => {
            socket.on(events.SHOW_SCREEN, (screen: GameScreen) => emit(showScreen(screen)));
            socket.on(events.START_MINIGAME, (data: MiniGameState) => emit(startMiniGame(data)));
            return () => {
                console.log("unsubscribed game events");
                socket.off(events.SHOW_SCREEN);
                socket.off(events.START_MINIGAME);
            };
        })
    );
    try {
        while (true) {
            const action: GameActionTypes = yield take(channel);
            yield put(action);
        }
    } finally {
        channel.close();
    }
}

function* joinGame(socket: SocketIOClient.Socket, gameId: string) {
    const channel: EventChannel<UpdateGameDataAction | JoinGameRejectedAction> = yield call(() =>
        eventChannel((emit) => {
            socket.on(events.JOIN_SUCCESS, (data: GameState) => {
                emit(updateGameData(data));
            });
            socket.on(events.JOIN_REJECTED, (reason: string) => emit(joinGameRejected(reason)));
            return () => {
                console.log("unsubscribed join room events");
                socket.off(events.JOIN_SUCCESS);
                socket.off(events.JOIN_REJECTED);
            };
        })
    );
    socket.emit(events.JOIN_GAME, gameId);
    try {
        const action: UpdateGameDataAction | JoinGameRejectedAction = yield take(channel);
        yield put(action);
        return action.type === actions.UPDATE_GAME_STATE;
    } finally {
        channel.close();
    }
}
