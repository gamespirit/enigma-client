import React, { FC } from "react";
import "./TeamComposition.css";
import { v4 as uuid } from "uuid";

interface Props {
    numberOfTeams: number;
    playersPerTeam: number;
}

const TeamComposition: FC<Props> = ({ numberOfTeams, playersPerTeam }) => {
    const icons = new Array(numberOfTeams)
        .fill(undefined)
        .map(() => (
            <div className={`team-icon sprite-player-icon-${playersPerTeam}`} key={uuid()}></div>
        ))
        .reduce<JSX.Element[]>(
            (prev, current, i) =>
                prev.concat(
                    current,
                    <div className="vs-text" key={uuid()}>
                        VS.
                    </div>
                ),
            []
        )
        .slice(0, numberOfTeams * 2 - 1);
    return <div className="team-composition">{icons}</div>;
};

export default TeamComposition;
