import { TweenLite } from "gsap";
import React, { FC, useEffect, useRef, useState } from "react";
import "./CountDownTimer.css";

interface Props {
    initialTime: number;
}

const CountDownTimer: FC<Props> = ({ initialTime: timer }) => {
    const [time, setTime] = useState(timer);
    const timerEl = useRef<HTMLDivElement>(null);
    useEffect(() => {
        let tick: number;
        let tween: gsap.core.Tween;
        if (time > 0) {
            tick = window.setTimeout(() => setTime(time - 1), 1000);
        }
        if (time >= 0) {
            tween = TweenLite.fromTo(
                timerEl.current,
                0.3,
                { opacity: 0.3, scale: 2.5 },
                { opacity: 1, scale: 1 }
            );
        }
        return () => {
            clearTimeout(tick);
            tween && tween.kill();
        };
    }, [time]);
    return (
        <div className="countdown-timer" ref={timerEl}>
            {time}
        </div>
    );
};

export default CountDownTimer;
