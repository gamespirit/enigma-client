import React, { FC, useState, useRef, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../store/reducers";
import "./Spectators.css";

function getShortName(name?: string): string {
    return name
        ? name
              .split(" ")
              .slice(0, 2)
              .map((fragment) => fragment[0].toUpperCase())
              .join("")
        : "";
}

const connector = connect((state: RootState) => ({
    spectators: state.app.room
        .data!.players.allIds.filter((id) => {
            if (id === state.app.session.uid!) {
                return false;
            }
            const roomData = state.app.room.data;
            if (roomData) {
                const teams = roomData.teams.byId;
                for (const teamId of roomData.teams.allIds) {
                    if (teams[teamId].players.includes(id)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        })
        .map((id) => {
            const playerData = state.app.room.data!.players.byId[id];
            return {
                name: playerData.name,
                ready: playerData.ready,
                id,
            };
        }),
    me: {
        shortName: getShortName(state.app.room.data!.players.byId[state.app.session.uid!].name),
        name: state.app.room.data!.players.byId[state.app.session.uid!].name,
        id: state.app.session.uid!,
        joinedTeam: !!state.app.room.data!.teams.allIds.find((teamId) =>
            state.app.room.data!.teams.byId[teamId].players.includes(state.app.session.uid!)
        ),
    },
}));

type Props = ConnectedProps<typeof connector>;

const Spectators: FC<Props> = ({ spectators, me }) => {
    const [meOpen, setMeOpen] = useState(false);
    const [othersOpen, setOthersOpen] = useState(false);
    const meButton = useRef<HTMLButtonElement>(null);
    const othersButton = useRef<HTMLButtonElement>(null);
    useEffect(() => {
        const closePopUps = (e: Event) => {
            if (e.target !== meButton.current) {
                setMeOpen(false);
            }
            if (e.target !== othersButton.current) {
                setOthersOpen(false);
            }
        };
        document.body.addEventListener("click", closePopUps);
        document.body.addEventListener("touchstart", closePopUps);
        return () => {
            document.body.removeEventListener("click", closePopUps);
            document.body.removeEventListener("touchstart", closePopUps);
        };
    }, []);
    return (
        <div className="spectators">
            {!me.joinedTeam ? (
                <div className="me">
                    <button
                        ref={meButton}
                        className="button button-round spectator-monogram me"
                        onClick={() => setMeOpen(true)}
                    >
                        {me.shortName}
                    </button>
                    <div className="full-name-popup" style={{ display: meOpen ? "block" : "none" }}>
                        <div className="spectator-full-name">{me.name}</div>
                    </div>
                </div>
            ) : null}
            {spectators.length ? (
                <div className="others">
                    <button
                        className="spectator-monogram others"
                        ref={othersButton}
                        onClick={() => setOthersOpen(true)}
                    >
                        <div className="icon sprite-player-icon-1-bg"></div>
                        <div className="players-not-joined-count">+{spectators.length}</div>
                    </button>
                    <div
                        className="full-name-popup"
                        style={{ display: othersOpen ? "block" : "none" }}
                    >
                        {spectators.map((player) => (
                            <div key={player.id} className="spectator-full-name">
                                {player.name}
                            </div>
                        ))}
                    </div>
                </div>
            ) : null}
        </div>
    );
};

export default connector(Spectators);
