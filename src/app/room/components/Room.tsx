import React, { FC, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import { RootState } from "../../../store/reducers";
import { joinRoom, leaveRoom } from "../roomActionCreators";
import "./Room.css";
import RoomFooterButtons from "./RoomFooterButtons";
import RoomHeader from "./RoomHeader";
import RoomTeams from "./RoomTeams";

type RouteProps = RouteComponentProps<{ roomId: string }>;

const connector = connect(
    (state: RootState, ownProps: RouteProps) => ({
        roomId: ownProps.match.params.roomId,
        data: state.app.room.data,
        joinedTeamId: state.app.room.data?.teams.allIds.find((teamId) =>
            state.app.room.data?.teams.byId[teamId].players.includes(state.app.session.uid!)
        ),
    }),
    {
        joinRoom,
        leaveRoom,
    }
);

type Props = ConnectedProps<typeof connector> & RouteProps;

const Room: FC<Props> = ({ roomId, data, joinedTeamId, joinRoom, leaveRoom }) => {
    useEffect(() => {
        joinRoom(roomId);
        return () => {
            leaveRoom();
        };
    }, [roomId, joinRoom, leaveRoom]);
    return (
        <>
            {data ? (
                <div className="room centerer">
                    <div className={`room-content ${joinedTeamId ? "joined" : ""}`}>
                        <RoomTeams joinedTeamId={joinedTeamId} />
                        <RoomHeader />
                        <RoomFooterButtons joinedTeamId={joinedTeamId} />
                    </div>
                </div>
            ) : null}
        </>
    );
};

export default withRouter(connector(Room));
