import React, { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { leaveTeam, ready, notReady } from "../roomActionCreators";
import { Button } from "../../buttons";
import { RootState } from "../../../store/reducers";

interface OwnProps {
    joinedTeamId?: string;
}

const connector = connect(
    (state: RootState, ownProps: OwnProps) => ({
        iAmReady: state.app.room.data?.players.byId[state.app.session.uid!].ready,
        countDownStarted: state.app.room.data?.countDownStarted,
        ...ownProps,
    }),
    {
        leaveTeam,
        ready,
        notReady,
    }
);

type Props = ConnectedProps<typeof connector>;

const RoomFooterButtons: FC<Props> = ({
    leaveTeam,
    ready,
    notReady,
    joinedTeamId,
    iAmReady,
    countDownStarted,
}) => {
    return joinedTeamId && !countDownStarted ? (
        <div className="bottom-buttons">
            <Button
                extraClass="leave-team-button"
                colorClass="neutral"
                onClick={() => leaveTeam(joinedTeamId)}
            >
                Leave team
            </Button>
            {!iAmReady ? (
                <Button extraClass="ready-button" colorClass="primary" onClick={ready}>
                    I'm ready
                </Button>
            ) : (
                <Button extraClass="unready-button" colorClass="neutral" onClick={notReady}>
                    I'm not ready
                </Button>
            )}
        </div>
    ) : null;
};

export default connector(RoomFooterButtons);
