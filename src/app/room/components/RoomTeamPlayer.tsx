import React, { FC } from "react";
import "./RoomTeamPlayer.css";

interface Props {
    ready: boolean;
    me: boolean;
    colorClass: string;
    countDownStarted: boolean;
}

const RoomTeamPlayer: FC<Props> = ({ ready, children, me, colorClass, countDownStarted }) => {
    const color = me ? "background" : colorClass;
    return (
        <div className={`team-player${me ? " me" : ""}`}>
            <div className="team-player-centerer">
                {ready ? (
                    countDownStarted ? null : (
                        <span className={`icon sprite-tick-${color}`}></span>
                    )
                ) : (
                    <span className={`icon spinner sprite-spinner-${color}`}></span>
                )}
                {children}
            </div>
        </div>
    );
};

export default RoomTeamPlayer;
