import React, { FC } from "react";
import "./RoundsIcon.css";

const RoundsIcon: FC = ({ children }) => {
    return (
        <div className="rounds">
            <span className="round-icon sprite-round-icon"></span>
            <span className="x-icon"></span>
            <span className="number-of-rounds">{children}</span>
        </div>
    );
};

export default RoundsIcon;
