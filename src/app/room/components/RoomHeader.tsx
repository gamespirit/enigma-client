import React, { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { BigTitle } from "../../window";
import TeamComposition from "./TeamComposition";
import Spectators from "./Spectators";
import history from "../../../services/history";
import RoundsIcon from "./RoundsIcon";
import CountDownTimer from "./CountDownTimer";
import { RootState } from "../../../store/reducers";

const connector = connect((state: RootState) => ({ data: state.app.room.data! }));

type Props = ConnectedProps<typeof connector>;

const RoomHeader: FC<Props> = ({ data }) => {
    return (
        <>
            <div className="room-title">
                <BigTitle>{data.name}</BigTitle>
            </div>
            {data.countDownStarted ? (
                <CountDownTimer initialTime={data.timer} />
            ) : (
                <>
                    <div className="room-info-display">
                        <TeamComposition
                            numberOfTeams={data.numberOfTeams}
                            playersPerTeam={data.numberOfTeams}
                        />
                        <div className="players-and-rounds">
                            <div className="players-joined">
                                Players: {data.players.allIds.length} /{" "}
                                {data.numberOfTeams * data.playersPerTeam}
                            </div>
                            <RoundsIcon>{data.rounds}</RoundsIcon>
                        </div>
                    </div>
                    <Spectators />
                    <button
                        className="button-round neutral exit-room-button"
                        onClick={() => {
                            history.push("/lobby");
                        }}
                    >
                        <div className="icon sprite-exit-icon"></div>
                    </button>
                </>
            )}
        </>
    );
};

export default connector(RoomHeader);
