import React, { FC } from "react";
import { BorderedWindow } from "../../window";
import "./RoomTeam.css";
import animals from "../../../store/animals";

interface Props {
    displayId: number;
    colorClass: string;
}

const RoomTeam: FC<Props> = ({ children, displayId, colorClass }) => {
    return (
        <div className={`room-team ${colorClass}`}>
            <BorderedWindow colorClass={colorClass}>
                <div className="team-name">
                    <span className="team-name-icon">{animals[displayId].icon}</span>
                    <span className="team-name-text">{animals[displayId].name}</span>
                </div>
                {children}
            </BorderedWindow>
        </div>
    );
};

export default RoomTeam;
