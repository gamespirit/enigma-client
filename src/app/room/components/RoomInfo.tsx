import React, { FC } from "react";
import "./RoomInfo.css";
import RoundsIcon from "./RoundsIcon";

interface Props {
    playersJoined: number;
    maxPlayers: number;
    rounds: number;
}

const RoomInfo: FC<Props> = ({ playersJoined, maxPlayers, rounds }) => {
    return (
        <div className="room-info">
            <div
                className="players-joined"
                style={playersJoined < maxPlayers ? { color: "var(--primary)" } : undefined}
            >{`${playersJoined} / ${maxPlayers}`}</div>
            <RoundsIcon>{rounds}</RoundsIcon>
        </div>
    );
};

export default RoomInfo;
