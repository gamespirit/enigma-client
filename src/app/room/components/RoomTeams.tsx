import React, { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { joinTeam } from "..";
import { RootState } from "../../../store/reducers";
import { Button } from "../../buttons";
import RoomTeam from "./RoomTeam";
import RoomTeamPlayer from "./RoomTeamPlayer";

interface OwnProps {
    joinedTeamId?: string;
}

const connector = connect(
    (state: RootState, ownProps: OwnProps) => ({
        data: state.app.room.data!,
        myUid: state.app.session.uid!,
        ...ownProps,
    }),
    {
        joinTeam,
    }
);

type Props = ConnectedProps<typeof connector>;

const RoomTeams: FC<Props> = ({ data, joinedTeamId, myUid, joinTeam }) => {
    const countDownStarted = data.countDownStarted;
    const colors = ["team-red", "team-blue"];
    const teamColors: { [teamId: string]: string } = {};
    let i = 0;
    for (const teamId of data.teams.allIds) {
        if (teamId === joinedTeamId) {
            teamColors[teamId] = "primary";
        } else {
            teamColors[teamId] = countDownStarted ? colors[i] : "neutral";
            i++;
        }
    }
    return (
        <>
            {data ? (
                <div className={`teams size-${data.numberOfTeams}`}>
                    {data.teams.allIds.map((teamId) => {
                        const team = data.teams.byId[teamId];
                        const full = team.players.length >= data.playersPerTeam;
                        const colorClass = teamColors[teamId];
                        return (
                            <RoomTeam
                                key={teamId}
                                displayId={team.displayId}
                                colorClass={colorClass}
                            >
                                {team.players.map((playerId) => {
                                    const me = playerId === myUid;
                                    const player = data.players.byId[playerId];
                                    return (
                                        <RoomTeamPlayer
                                            ready={player.ready}
                                            key={playerId}
                                            me={me}
                                            colorClass={colorClass}
                                            countDownStarted={countDownStarted}
                                        >
                                            {player.name}
                                        </RoomTeamPlayer>
                                    );
                                })}
                                {!full && !joinedTeamId ? (
                                    <Button colorClass="primary" onClick={() => joinTeam(teamId)}>
                                        Join team
                                    </Button>
                                ) : null}
                            </RoomTeam>
                        );
                    })}
                </div>
            ) : null}
        </>
    );
};

export default connector(RoomTeams);
