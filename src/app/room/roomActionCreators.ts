import * as actions from "./roomActions";
import { RoomActionTypes } from "./roomActionTypes";
import { RoomData } from "./roomStateTypes";

export const joinRoom = (roomId: string): RoomActionTypes => ({
    type: actions.JOIN_ROOM,
    roomId,
});

export const leaveRoom = (): RoomActionTypes => ({
    type: actions.LEAVE_ROOM,
});

export const joinTeam = (teamId: string): RoomActionTypes => ({
    type: actions.JOIN_TEAM,
    teamId,
});

export const leaveTeam = (teamId: string): RoomActionTypes => ({
    type: actions.LEAVE_TEAM,
    teamId,
});

export const ready = (): RoomActionTypes => ({
    type: actions.READY,
});

export const notReady = (): RoomActionTypes => ({
    type: actions.NOT_READY,
});

export const joinRoomSuccess = (data: RoomData): RoomActionTypes => ({
    type: actions.JOIN_ROOM_SUCCESS,
    data,
});

export const joinRoomReject = (reason: string): RoomActionTypes => ({
    type: actions.JOIN_ROOM_REJECTED,
    reason,
});

export const playerLeftRoom = (uid: string): RoomActionTypes => ({
    type: actions.PLAYER_LEFT_ROOM,
    uid,
});

export const playerJoinedRoom = (uid: string, name: string): RoomActionTypes => ({
    type: actions.PLAYER_JOINED_ROOM,
    name,
    uid,
});

export const playerJoinedTeam = (uid: string, teamId: string): RoomActionTypes => ({
    type: actions.PLAYER_JOIN_TEAM,
    teamId,
    uid,
});

export const playerLeftTeam = (uid: string, teamId: string): RoomActionTypes => ({
    type: actions.PLAYER_LEAVE_TEAM,
    teamId,
    uid,
});

export const playerReady = (uid: string): RoomActionTypes => ({
    type: actions.PLAYER_READY,
    uid,
});

export const playerNotReady = (uid: string): RoomActionTypes => ({
    type: actions.PLAYER_NOT_READY,
    uid,
});

export const startCountDown = (timer: number): RoomActionTypes => ({
    type: actions.START_COUNTDOWN,
    timer,
});

export const cancelCountDown = (): RoomActionTypes => ({
    type: actions.CANCEL_COUNTDOWN,
});
