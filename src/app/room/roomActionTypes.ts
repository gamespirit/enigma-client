import { RoomData } from "./roomStateTypes";
import * as actions from "./roomActions";

export interface StartCountDownAction {
    type: typeof actions.START_COUNTDOWN;
    timer: number;
}

export interface CancelCountDownAction {
    type: typeof actions.CANCEL_COUNTDOWN;
}

export interface PlayerReadyAction {
    type: typeof actions.PLAYER_READY;
    uid: string;
}

export interface PlayerNotReadyAction {
    type: typeof actions.PLAYER_NOT_READY;
    uid: string;
}

export interface PlayerJoinedRoomAciton {
    type: typeof actions.PLAYER_JOINED_ROOM;
    uid: string;
    name: string;
}

export interface PlayerLeftRoomAciton {
    type: typeof actions.PLAYER_LEFT_ROOM;
    uid: string;
}

export interface JoinRoomAction {
    type: typeof actions.JOIN_ROOM;
    roomId: string;
}

export interface LeaveRoomAction {
    type: typeof actions.LEAVE_ROOM;
}

export interface JoinRoomRejectAction {
    type: typeof actions.JOIN_ROOM_REJECTED;
    reason: string;
}

export interface JoinRoomSuccessAction {
    type: typeof actions.JOIN_ROOM_SUCCESS;
    data: RoomData;
}

export interface JoinTeamAction {
    type: typeof actions.JOIN_TEAM;
    teamId: string;
}

export interface LeaveTeamAction {
    type: typeof actions.LEAVE_TEAM;
    teamId: string;
}

export interface ReadyAction {
    type: typeof actions.READY;
}

export interface NotReadyAction {
    type: typeof actions.NOT_READY;
}

export interface PlayerJoinedTeamAction {
    type: typeof actions.PLAYER_JOIN_TEAM;
    teamId: string;
    uid: string;
}

export interface PlayerLeftTeamAction {
    type: typeof actions.PLAYER_LEAVE_TEAM;
    teamId: string;
    uid: string;
}

export type RoomActionTypes =
    | JoinRoomAction
    | LeaveRoomAction
    | ReadyAction
    | NotReadyAction
    | JoinTeamAction
    | LeaveTeamAction
    | JoinRoomRejectAction
    | JoinRoomSuccessAction
    | PlayerJoinedTeamAction
    | PlayerLeftTeamAction
    | PlayerJoinedRoomAciton
    | PlayerLeftRoomAciton
    | PlayerReadyAction
    | PlayerNotReadyAction
    | StartCountDownAction
    | CancelCountDownAction;
