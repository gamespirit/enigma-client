import { RoomState } from "./roomStateTypes";
import { produce } from "immer";
import { RoomActionTypes } from "./roomActionTypes";
import * as actions from "./roomActions";

const initialState: RoomState = {
    isJoiningRoom: false,
};

export const roomReducer = produce((draft: RoomState, action: RoomActionTypes) => {
    switch (action.type) {
        case actions.JOIN_ROOM:
            delete draft.error;
            draft.isJoiningRoom = true;
            break;
        case actions.LEAVE_ROOM:
            draft.isJoiningRoom = false;
            delete draft.data;
            break;
        case actions.JOIN_ROOM_SUCCESS:
            draft.isJoiningRoom = false;
            draft.data = action.data;
            break;
        case actions.JOIN_ROOM_REJECTED:
            draft.isJoiningRoom = false;
            draft.error = action.reason;
            break;
        case actions.PLAYER_JOIN_TEAM: {
            draft.data?.teams.byId[action.teamId].players.push(action.uid);
            break;
        }
        case actions.PLAYER_LEAVE_TEAM: {
            const player = draft.data?.players.byId[action.uid];
            if (player) {
                player.ready = false;
            }
            const team = draft.data?.teams.byId[action.teamId];
            if (team) {
                team.players = team?.players.filter((id) => id !== action.uid);
            }
            break;
        }
        case actions.PLAYER_LEFT_ROOM: {
            const players = draft.data?.players;
            if (players) {
                delete players.byId[action.uid];
                players.allIds = players.allIds.filter((id) => id !== action.uid);
            }
            const teams = draft.data?.teams;
            if (teams) {
                for (const teamId of teams.allIds) {
                    teams.byId[teamId].players = teams.byId[teamId].players.filter(
                        (id) => id !== action.uid
                    );
                }
            }
            break;
        }
        case actions.PLAYER_JOINED_ROOM: {
            const players = draft.data?.players;
            if (players) {
                players.byId[action.uid] = {
                    name: action.name,
                    ready: false,
                };
                players.allIds.push(action.uid);
            }
            break;
        }
        case actions.PLAYER_READY: {
            if (draft.data) {
                draft.data.players.byId[action.uid].ready = true;
            }
            break;
        }
        case actions.PLAYER_NOT_READY: {
            if (draft.data) {
                draft.data.players.byId[action.uid].ready = false;
            }
            break;
        }
        case actions.START_COUNTDOWN: {
            if (draft.data) {
                draft.data.countDownStarted = true;
                draft.data.timer = action.timer;
            }
            break;
        }
        case actions.CANCEL_COUNTDOWN: {
            if (draft.data) {
                draft.data.countDownStarted = false;
            }
            break;
        }
    }
}, initialState);
