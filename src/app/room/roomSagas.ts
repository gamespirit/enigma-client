import { EventChannel, eventChannel } from "redux-saga";
import { call, fork, put, race, take } from "redux-saga/effects";
import { joinRoomReject, joinRoomSuccess } from ".";
import { listenForSocketEventOnce } from "../../generatorHelpers";
import history from "../../services/history";
import {
    cancelCountDown,
    playerJoinedRoom,
    playerJoinedTeam,
    playerLeftRoom,
    playerLeftTeam,
    playerNotReady,
    playerReady,
    startCountDown,
} from "./roomActionCreators";
import * as actions from "./roomActions";
import {
    JoinRoomAction,
    JoinRoomRejectAction,
    JoinRoomSuccessAction,
    JoinTeamAction,
    LeaveTeamAction,
    RoomActionTypes,
} from "./roomActionTypes";
import * as events from "./roomEvents";
import { RoomData } from "./roomStateTypes";

export function* roomFlow(socket: SocketIOClient.Socket) {
    while (true) {
        const { roomId }: JoinRoomAction = yield take(actions.JOIN_ROOM);
        const success: string = yield call(joinRoom, socket, roomId);
        if (success) {
            yield race({
                listenForRoomEvents: call(listenForRoomEvents, socket),
                emitRoomAcitons: call(emitRoomAcitons, socket),
                listenForGameReady: call(listenForGameReady, socket, roomId),
                leaveRoom: take(actions.LEAVE_ROOM),
            });
            socket.emit(events.LEAVE_ROOM);
        } else {
            history.push("/lobby");
        }
    }
}

function* listenForGameReady(socket: SocketIOClient.Socket, roomId: string) {
    yield call(listenForSocketEventOnce, socket, {
        name: events.GAME_READY,
        handler: function () {
            history.push(`/game/${roomId}`);
        },
    });
}

function* joinRoom(socket: SocketIOClient.Socket, roomId: string) {
    const channel: EventChannel<JoinRoomSuccessAction | JoinRoomRejectAction> = yield call(() =>
        eventChannel((emit) => {
            socket.on(events.ROOM_JOIN_SUCCESS, (data: RoomData) => {
                emit(joinRoomSuccess(data));
            });
            socket.on(events.ROOM_JOIN_REJECT, (reason: string) => emit(joinRoomReject(reason)));
            return () => {
                console.log("unsubscribed join room events");
                socket.off(events.ROOM_JOIN_SUCCESS);
                socket.off(events.ROOM_JOIN_REJECT);
            };
        })
    );
    socket.emit(events.JOIN_ROOM, roomId);
    try {
        const action: JoinRoomSuccessAction | JoinRoomRejectAction = yield take(channel);
        yield put(action);
        return action.type === actions.JOIN_ROOM_SUCCESS;
    } finally {
        channel.close();
    }
}

function* emitRoomAcitons(socket: SocketIOClient.Socket) {
    yield fork(emitJoinTeam, socket);
    yield fork(emitLeaveTeam, socket);
    yield fork(emitReady, socket);
    yield fork(emitNotReady, socket);
}

function* emitReady(socket: SocketIOClient.Socket) {
    while (true) {
        yield take(actions.READY);
        socket.emit(events.READY);
    }
}

function* emitNotReady(socket: SocketIOClient.Socket) {
    while (true) {
        yield take(actions.NOT_READY);
        socket.emit(events.UNREADY);
    }
}

function* emitLeaveTeam(socket: SocketIOClient.Socket) {
    while (true) {
        const action: LeaveTeamAction = yield take(actions.LEAVE_TEAM);
        socket.emit(events.LEAVE_TEAM, action.teamId);
    }
}

function* emitJoinTeam(socket: SocketIOClient.Socket) {
    while (true) {
        const action: JoinTeamAction = yield take(actions.JOIN_TEAM);
        socket.emit(events.JOIN_TEAM, action.teamId);
    }
}

function* listenForRoomEvents(socket: SocketIOClient.Socket) {
    const channel: EventChannel<RoomActionTypes> = yield call(() =>
        eventChannel((emit) => {
            socket.on(events.JOIN_ROOM, (uid: string, name: string) =>
                emit(playerJoinedRoom(uid, name))
            );
            socket.on(events.LEAVE_ROOM, (uid: string) => emit(playerLeftRoom(uid)));
            socket.on(events.JOIN_TEAM, (uid: string, teamId: string) =>
                emit(playerJoinedTeam(uid, teamId))
            );
            socket.on(events.LEAVE_TEAM, (uid: string, teamId: string) =>
                emit(playerLeftTeam(uid, teamId))
            );
            socket.on(events.READY, (uid: string) => emit(playerReady(uid)));
            socket.on(events.UNREADY, (uid: string) => emit(playerNotReady(uid)));
            socket.on(events.START_COUNTDOWN, (seconds: number) => emit(startCountDown(seconds)));
            socket.on(events.CANCEL_COUNTDOWN, () => emit(cancelCountDown()));
            return () => {
                console.log("unsubscribed room events");
                socket.off(events.JOIN_ROOM);
                socket.off(events.LEAVE_ROOM);
                socket.off(events.JOIN_TEAM);
                socket.off(events.LEAVE_TEAM);
                socket.off(events.READY);
                socket.off(events.UNREADY);
                socket.off(events.START_COUNTDOWN);
                socket.off(events.CANCEL_COUNTDOWN);
            };
        })
    );
    try {
        while (true) {
            yield put(yield take(channel));
        }
    } finally {
        channel.close();
    }
}
