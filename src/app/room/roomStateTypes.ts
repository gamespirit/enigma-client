import { LobbyRoomState } from "../lobby/lobbyStateTypes";

export interface RoomState {
    data?: RoomData;
    error?: string;
    isJoiningRoom: boolean;
}

export interface RoomData {
    id: string;
    name: string;
    rounds: number;
    numberOfTeams: number;
    playersPerTeam: number;
    countDownStarted: boolean;
    timer: number;
    players: {
        byId: { [id: string]: RoomPlayer };
        allIds: string[];
    };
    teams: {
        byId: {
            [id: string]: RoomTeamData;
        };
        allIds: string[];
    };
}

export interface RoomTeamData {
    displayId: number;
    players: string[];
}

export interface RoomPlayer {
    name: string;
    ready: boolean;
}
