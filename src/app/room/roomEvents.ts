export const JOIN_ROOM = "ROOM/JOIN";
export const LEAVE_ROOM = "ROOM/LEAVE";
export const ROOM_JOIN_SUCCESS = "ROOM/JOIN_SUCCESS";
export const ROOM_JOIN_REJECT = "ROOM/JOIN_REJECT";
export const JOIN_TEAM = "ROOM/JOIN_TEAM";
export const LEAVE_TEAM = "ROOM/LEAVE_TEAM";
export const READY = "ROOM/READY";
export const UNREADY = "ROOM/UNREADY";
export const START_COUNTDOWN = "ROOM/START_COUNTDOWN";
export const CANCEL_COUNTDOWN = "ROOM/CANCEL_COUNTDOWN";
export const GAME_READY = "ROOM/GAME_READY";
