import React, { FC } from "react";
import "./Button.css";

interface Props {
    colorClass: string;
    extraClass?: string;
    busyText?: string;
    disabled?: boolean;
    busy?: boolean;
    type?: "button" | "submit" | "reset" | undefined;
    onClick?: () => void;
}

const Button: FC<Props> = ({
    children,
    colorClass,
    extraClass,
    busyText,
    disabled,
    onClick,
    busy,
    type,
}) => {
    const buttonClass =
        (extraClass ? extraClass : "") + (busy ? " busy" : "") + (disabled ? " disabled" : "");
    return (
        <button
            className={`button button-${colorClass} ${buttonClass}`}
            onClick={onClick}
            disabled={disabled || busy}
            type={type || "button"}
        >
            <div className={`button-body sprite-button-${colorClass} normal`}></div>
            <div className={`button-body sprite-button-${colorClass}-hover hover`}></div>
            <div className={`button-body sprite-button-${colorClass}-active active`}></div>
            <div className={`button-body right sprite-button-busy busy`}></div>
            <div className={`button-body right sprite-button-disabled disabled`}></div>
            <div className={`button-side left sprite-button-${colorClass} normal`}></div>
            <div className={`button-side right sprite-button-${colorClass} normal`}></div>
            <div className={`button-side left sprite-button-${colorClass}-hover hover`}></div>
            <div className={`button-side right sprite-button-${colorClass}-hover hover`}></div>
            <div className={`button-side left sprite-button-${colorClass}-active active`}></div>
            <div className={`button-side right sprite-button-${colorClass}-active active`}></div>
            <div className={`button-side left sprite-button-busy busy`}></div>
            <div className={`button-side right sprite-button-busy busy`}></div>
            <div className={`button-side left sprite-button-disabled disabled`}></div>
            <div className={`button-side right sprite-button-disabled disabled`}></div>
            <span className="content">{busy ? busyText || children : children}</span>
        </button>
    );
};

export default Button;
