import { ServiceType } from "../../services/serviceTypes";

export interface SessionState {
    isAuthenticating: boolean;
    connected: boolean;
    initialized: boolean;
    isConnecting: boolean;
    userName?: string;
    tokenId?: string;
    error?: string;
    uid?: string;
    service?: ServiceType;
}
