import { SessionState } from "./sessionStateTypes";
import * as actions from "./sessionActions";
import { SessionAcitonTypes } from "./sessionActionTypes";

const initialState: SessionState = {
    isAuthenticating: false,
    connected: false,
    isConnecting: false,
    initialized: false,
};

export function sessionReducer(state = initialState, action: SessionAcitonTypes): SessionState {
    switch (action.type) {
        case actions.LOG_IN_START:
            return {
                ...state,
                isAuthenticating: true,
                service: action.service,
            };
        case actions.LOAD_SESSION:
            return {
                ...initialState,
                ...action.data,
            };
        case actions.LOG_IN_SUCCESS:
            return {
                ...state,
                userName: action.user,
                isAuthenticating: false,
                tokenId: action.token,
                connected: false,
                isConnecting: true,
            };
        case actions.LOG_IN_FAIL:
            return {
                ...initialState,
                error: action.error,
            };
        case actions.CONNECT_START:
            return {
                ...state,
                isConnecting: true,
            };
        case actions.CONNECT_SUCCESS:
            return {
                ...state,
                isAuthenticating: false,
                isConnecting: false,
                connected: true,
                uid: action.uid,
            };
        case actions.CONNECT_FAIL:
            return {
                ...initialState,
                userName: state.userName,
                error: action.error,
            };
        case actions.INITIALIZE_APP:
            return {
                ...state,
                initialized: true,
            };
        case actions.LOG_OUT:
            return initialState;
        default:
            return state;
    }
}
