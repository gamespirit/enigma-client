import { SessionState } from ".";
import { ServiceType } from "../../services/serviceTypes";
import * as actions from "./sessionActions";

export interface StartLoginAction {
    type: typeof actions.LOG_IN_START;
    userName?: string;
    service: ServiceType;
}

export interface LoadSessionAction {
    type: typeof actions.LOAD_SESSION;
    data?: SessionState;
}

export interface LogInSuccessAction {
    type: typeof actions.LOG_IN_SUCCESS;
    user: string;
    token: string;
}

export interface LogInFailAction {
    type: typeof actions.LOG_IN_FAIL;
    error: string;
}

export interface LogOutAction {
    type: typeof actions.LOG_OUT;
}

export interface ConnectStartAction {
    type: typeof actions.CONNECT_START;
}

export interface ConnectSuccessAction {
    type: typeof actions.CONNECT_SUCCESS;
    uid: string;
}

export interface ConnectFailAction {
    type: typeof actions.CONNECT_FAIL;
    error: string;
}

export interface InitializeAppAction {
    type: typeof actions.INITIALIZE_APP;
}
export type SessionAcitonTypes =
    | StartLoginAction
    | LoadSessionAction
    | LogInSuccessAction
    | LogInFailAction
    | LogOutAction
    | InitializeAppAction
    | ConnectStartAction
    | ConnectSuccessAction
    | ConnectFailAction;
