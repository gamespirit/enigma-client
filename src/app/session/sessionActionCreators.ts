import { SessionState } from ".";
import { ServiceType } from "../../services/serviceTypes";
import * as actions from "./sessionActions";
import {
    ConnectFailAction,
    ConnectStartAction,
    ConnectSuccessAction,
    InitializeAppAction,
    LoadSessionAction,
    LogInFailAction,
    LogInSuccessAction,
    LogOutAction,
    StartLoginAction,
} from "./sessionActionTypes";

export const loadSession = (data?: SessionState): LoadSessionAction => ({
    type: actions.LOAD_SESSION,
    data,
});

export const startConnect = (): ConnectStartAction => ({
    type: actions.CONNECT_START,
});

export const startLogin = (service: ServiceType, userName?: string): StartLoginAction => ({
    type: actions.LOG_IN_START,
    userName,
    service,
});

export const loginFailed = (error: string): LogInFailAction => ({
    type: actions.LOG_IN_FAIL,
    error,
});

export const loginSuccess = (token: string, user: string): LogInSuccessAction => ({
    type: actions.LOG_IN_SUCCESS,
    token,
    user,
});

export const disconnect = (error: string): ConnectFailAction => ({
    type: actions.CONNECT_FAIL,
    error,
});

export const connectSuccess = (uid: string): ConnectSuccessAction => ({
    type: actions.CONNECT_SUCCESS,
    uid,
});

export const logOut = (): LogOutAction => ({
    type: actions.LOG_OUT,
});

export const initializeApp = (): InitializeAppAction => ({
    type: actions.INITIALIZE_APP,
});
