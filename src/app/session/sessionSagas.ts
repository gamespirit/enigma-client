import firebase from "firebase/app";
import { EventChannel, eventChannel } from "redux-saga";
import { call, put, select, take } from "redux-saga/effects";
import { fbAuthProvider } from "../../services/firebase";
import { SERVICE_ANONYMUS_LOGIN, SERVICE_FB_LOGIN } from "../../services/serviceTypes";
import { RootState } from "../../store/reducers";
import * as actions from "./sessionActionCreators";
import { startLogin } from "./sessionActionCreators";
import { CONNECT_SUCCESS, LOG_IN_START, LOG_OUT } from "./sessionActions";
import { ConnectFailAction, ConnectSuccessAction, StartLoginAction } from "./sessionActionTypes";
import * as events from "./sessionEvents";
import { SessionState } from "./sessionStateTypes";
import io from "socket.io-client";

/** Need to run this after redirect has ended from facebook */
function* getFirebaseAuthRedirectResult() {
    try {
        const auth = firebase.auth();
        const credential: firebase.auth.UserCredential = yield call([auth, auth.getRedirectResult]);

        if (!credential || !credential.user || !credential.user.displayName) {
            // log in user name and credentials not present
            yield call(failLogin, "Log in interrupted, please try again.");
        } else {
            const token = yield call([credential.user, credential.user.getIdToken]);
            // user succesfully signed in
            yield call(saveLogin, token, credential.user.displayName);
        }
    } catch (error) {
        console.log(error);
        yield call(failLogin, error);
    }
}

function* failLogin(error: string) {
    yield put(actions.loginFailed(error));
    sessionStorage.removeItem("session");
}

function* saveLogin(tokenId: string, userName: string) {
    yield put(actions.loginSuccess(tokenId, userName));
    yield call(saveSessionToStorage);
}

function subscribeToConnectEvents(socket: SocketIOClient.Socket, timeout: number) {
    return eventChannel((emit) => {
        socket.on(events.SOCKET_CONNECT, () => {
            const timer = setTimeout(() => {
                emit(actions.disconnect("Socket authentication timed out."));
            }, timeout);
            socket.on(events.AUTHENTICATED, (uid: string) => {
                clearTimeout(timer);
                emit(actions.connectSuccess(uid));
            });
            socket.on(events.AUTHENTICATION_FAILED, (reason: string) =>
                emit(actions.disconnect(reason))
            );
        });

        socket.on(events.SOCKET_CONNECT_TIMEOUT, () =>
            emit(actions.disconnect("Connection to server timed out."))
        );

        socket.on(events.SOCKET_CONNECT_ERROR, (error: string) =>
            emit(actions.disconnect("Failed to connect to server. Reason: " + error))
        );

        return () => {
            // cleanup
            socket.off(events.SOCKET_CONNECT);
            socket.off(events.AUTHENTICATED);
            socket.off(events.AUTHENTICATION_FAILED);
            socket.off(events.SOCKET_CONNECT_TIMEOUT);
            socket.off(events.SOCKET_CONNECT_ERROR);
        };
    });
}

function subscribeToDisconnectEvents(socket: SocketIOClient.Socket) {
    return eventChannel((emit) => {
        socket.on(events.SOCKET_ERROR, (error: string) => {
            // handle errors
            console.log(error);
            emit("Disconnected from server. Reason: " + error);
        });

        socket.on(events.SOCKET_DISCONNECT, (reason: string) =>
            emit("Disconnected from server. Reason: " + reason)
        );

        return () => {
            socket.off(events.SOCKET_ERROR);
            socket.off(events.SOCKET_DISCONNECT);
            socket.disconnect();
        };
    });
}

/** Connect to socket server and check if user is authenticated. Saves connected status to the store if successful */
function* waitUntilConnected() {
    const session: SessionState = yield select((state: RootState) => state.app.session);
    const userName: string = session.userName!;
    const tokenId: string = session.tokenId!;
    yield put(actions.startConnect());

    const timeout = 5000;
    const options: SocketIOClient.ConnectOpts = {
        query: {
            userName,
            tokenId,
        },
        timeout,
        reconnection: false,
    };

    const url =
        process.env.NODE_ENV !== "production"
            ? `http://${window.location.hostname}:5000/`
            : `https://gamespirit-enigma-server.herokuapp.com/`;

    const socket = io(url, options);
    const connectChannel: EventChannel<ConnectFailAction | ConnectSuccessAction> = yield call(
        subscribeToConnectEvents,
        socket,
        timeout
    );
    try {
        const action: ConnectFailAction | ConnectSuccessAction = yield take(connectChannel);
        yield put(action);
        yield call(saveSessionToStorage);
        if (action.type === CONNECT_SUCCESS) {
            return socket;
        }
    } finally {
        connectChannel.close();
    }
}

function* loadSessionFromStorage() {
    const dataStr = sessionStorage.getItem("session");
    const data: SessionState | undefined = dataStr ? JSON.parse(dataStr) : undefined;
    yield put(actions.loadSession(data));
    return data;
}

function* saveSessionToStorage() {
    const session: SessionState = yield select((state: RootState) => state.app.session);
    const { tokenId, userName, isAuthenticating, service } = session;
    sessionStorage.setItem(
        "session",
        JSON.stringify({ tokenId, userName, isAuthenticating, service })
    );
}

/** Logging in to facebook will redirect to facebook log in page */
async function fbLoginWithRedirect() {
    try {
        return firebase.auth().signInWithRedirect(fbAuthProvider);
    } catch (reason) {
        console.log(reason);
        return { error: reason.message };
    }
}

/** Starts facebook login with redirect */
function* startFBLoginWithRedirect() {
    yield put(startLogin(SERVICE_FB_LOGIN));
    yield call(saveSessionToStorage);
    yield call(fbLoginWithRedirect);
}

/** Starts anonymus login */
function* startAnonymusLogin(userName: string) {
    yield put(startLogin(SERVICE_ANONYMUS_LOGIN));
    const auth = firebase.auth();
    try {
        yield call([auth, auth.signInAnonymously]);
        const token: string = yield call(waitForAuthTofinish);
        yield call(saveLogin, token, userName);
    } catch (error) {
        console.log(error);
        yield call(failLogin, error);
    }
}

/** Starts firebase anonymus login sequence */
const waitForAuthTofinish = () =>
    new Promise((resolve, reject) => {
        firebase.auth().onAuthStateChanged(function (firebaseUser) {
            if (firebaseUser) {
                // User is signed in.
                // const uid = firebaseUser.uid;
                firebaseUser.getIdToken().then((token) => resolve(token));
            } else {
                reject("User is signed out.");
            }
        });
    });

/** Finishes when the socket is disconnected. */
export function* watchDisconnect(socket: SocketIOClient.Socket) {
    const channel: EventChannel<string> = yield call(subscribeToDisconnectEvents, socket);
    try {
        const error = yield take(channel);
        yield put(actions.disconnect(error));
    } finally {
        channel.close();
    }
}

/** Tries logging in until the user is successfully logged in. */
function* tryLoginUntilSuccessful() {
    let session: SessionState = yield select((state: RootState) => state.app.session);
    while (!session.tokenId) {
        // if not logged in, try to log in until successful
        const action: StartLoginAction = yield take(LOG_IN_START);
        yield put(action);
        switch (action.service) {
            case SERVICE_FB_LOGIN: {
                yield call(startFBLoginWithRedirect); // will reload the page once complete
                break;
            }
            case SERVICE_ANONYMUS_LOGIN: {
                if (action.userName && action.userName !== "") {
                    yield call(startAnonymusLogin, action.userName);
                } else {
                    yield put(actions.loginFailed("Username must not be empty!"));
                }
                break;
            }
        }
        session = yield select((state: RootState) => state.app.session);
    }
}

/** Tries to connect if session indicates the user is authenticated, and falls back to login if not until the user is succesfully logged in. */
export function* createSession() {
    // by this time session exists
    while (true) {
        // try to load session OR log in with one of the provided methods
        yield call(tryLoginUntilSuccessful);

        // userName and tokenId must exist here
        // if connect fails, the try log in again
        const socket = yield call(waitUntilConnected);
        if (socket) {
            // successfully connected
            return socket;
        }
    }
}

/** Loads session from localstorage, and checks if there is a redirect from facebook login and saves that to the store. */
export function* loadSession() {
    // check if user is already authenticated
    const savedSession: SessionState = yield call(loadSessionFromStorage);
    if (savedSession?.isAuthenticating && savedSession?.service === SERVICE_FB_LOGIN) {
        // facebook redirect complete, check results
        yield call(getFirebaseAuthRedirectResult);
    }
}

/** Watches for logout action. */
export function* watchLogout(socket: SocketIOClient.Socket) {
    yield put(yield take(LOG_OUT));
    sessionStorage.removeItem("session");
    socket.disconnect();
}
