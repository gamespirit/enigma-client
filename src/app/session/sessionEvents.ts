export const AUTHENTICATED = "PLAYER/AUTHENTICATED";
export const AUTHENTICATION_FAILED = "PLAYER/AUTHENTICATION_FAILED";
export const SOCKET_CONNECT = "connect";
export const SOCKET_ERROR = "error";
export const SOCKET_DISCONNECT = "disconnect";
export const SOCKET_CONNECT_ERROR = "connect_error";
export const SOCKET_CONNECT_TIMEOUT = "connect_timeout";
