import { combineReducers } from "redux";
import { lobbyReducer } from "./lobby/lobbyReducer";
import { roomReducer } from "./room";
import { sessionReducer } from "./session";

export const appReducer = combineReducers({
    session: sessionReducer,
    lobby: lobbyReducer,
    room: roomReducer,
});
