import { appReducer } from "./appReducer";
import "../services/gsap";

export { default as App } from "./components/App";
export { appReducer };
