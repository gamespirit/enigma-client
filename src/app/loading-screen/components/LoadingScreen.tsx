import React, { FC } from "react";
import Rotor from "./Rotor";
import "./LoadingScreen.css";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../store/reducers";

const connector = connect((state: RootState) => ({
    show:
        state.app.session.isAuthenticating ||
        state.app.session.isConnecting ||
        state.app.room.isJoiningRoom,
    text: state.app.session.isAuthenticating
        ? "Authenticating..."
        : state.app.session.isConnecting
        ? "Connecting to server..."
        : state.app.room.isJoiningRoom
        ? "Joining room.."
        : "",
}));

type Props = ConnectedProps<typeof connector>;

const LoadingScreen: FC<Props> = ({ text, show }) => {
    return (
        <div className="loading-screen" style={{ display: show ? "block" : "none" }}>
            <div className="centerer">
                <div className="loading-screen-content">
                    <Rotor speed={4} />
                    <Rotor speed={3} />
                    <Rotor speed={2} />
                    <Rotor speed={1} />
                    <div className="text">{text}</div>
                </div>
            </div>
        </div>
    );
};

export default connector(LoadingScreen);
