import React, { FC } from "react";
import "./Rotor.css";

interface Props {
    speed: number;
}

const Rotor: FC<Props> = ({ speed }) => {
    return (
        <div className="rotor">
            <div className="rotor-marker sprite-rotor-marker"></div>
            <div
                className="rotor-disc sprite-rotor"
                style={{ animationDuration: `${6 / speed}s` }}
            ></div>
        </div>
    );
};

export default Rotor;
