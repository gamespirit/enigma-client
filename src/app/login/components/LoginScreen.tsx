import React, { ChangeEvent, FC, useRef, useState, FormEvent } from "react";
import { connect, ConnectedProps } from "react-redux";
import { SERVICE_ANONYMUS_LOGIN, SERVICE_FB_LOGIN } from "../../../services/serviceTypes";
import { RootState } from "../../../store/reducers";
import { Button } from "../../buttons";
import { startLogin } from "../../session";
import { ReactComponent as FbIcon } from "../assets/svg/fb-icon.svg";
import logo from "../assets/svg/logo.svg";
import "./LoginScreen.css";

const connector = connect(
    (state: RootState) => ({
        error: state.app.session.error,
    }),
    { startLogin }
);

type Props = ConnectedProps<typeof connector>;

const LoginScreen: FC<Props> = ({ startLogin, error }) => {
    const nameInputEl = useRef<HTMLInputElement>(null);
    const [user, setUser] = useState<string>("");

    const handleSubmit = (e: FormEvent): void => {
        e.preventDefault();
        startLogin(SERVICE_ANONYMUS_LOGIN, user.trim());
    };

    const loginWithFb = () => {
        startLogin(SERVICE_FB_LOGIN);
    };

    return (
        <div className="login-screen-container centerer">
            <div className="login-screen">
                <img src={logo} alt="logo" className="logo" />
                {error ? <div className="error">{error}</div> : null}
                <div className="login-form-container">
                    <form onSubmit={handleSubmit} className="form">
                        <input
                            type="text"
                            placeholder="Enter your name"
                            required
                            ref={nameInputEl}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => setUser(e.target.value)}
                            value={user}
                        />
                        <Button colorClass="primary" busyText="Connecting server.." type="submit">
                            Guest Sign In
                        </Button>
                    </form>
                    <Button colorClass="fb" onClick={loginWithFb} busyText="Please Wait..">
                        <FbIcon className="fb-icon" />
                        Sign In
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default connector(LoginScreen);
