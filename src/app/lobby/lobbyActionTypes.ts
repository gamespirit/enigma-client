import { CreateRoomPayload, LobbyRoomState } from "./lobbyStateTypes";
import * as actions from "./lobbyActions";

interface LobbyUpdateAction {
    type: typeof actions.ON_UPDATE_ROOM_LIST;
    rooms: LobbyRoomState[];
}

interface ActivateSearchAction {
    type: typeof actions.ACTIVATE_SEARCH;
}

interface DeactivateSearchAction {
    type: typeof actions.DEACTIVATE_SEARCH;
}

interface SetSearchQueryAction {
    type: typeof actions.SET_SEARCH_QUERY;
    query: string;
}

interface JoinLobbyAction {
    type: typeof actions.JOIN_LOBBY;
}

interface LeaveLobbyAction {
    type: typeof actions.LEAVE_LOBBY;
}

export interface CreateRoomAction {
    type: typeof actions.CREATE_ROOM;
    roomParams: CreateRoomPayload;
}

export interface RoomCreatedAction {
    type: typeof actions.ROOM_CREATED;
    roomId: string;
}

export type LobbyActionTypes =
    | LobbyUpdateAction
    | ActivateSearchAction
    | DeactivateSearchAction
    | JoinLobbyAction
    | LeaveLobbyAction
    | CreateRoomAction
    | RoomCreatedAction
    | SetSearchQueryAction;
