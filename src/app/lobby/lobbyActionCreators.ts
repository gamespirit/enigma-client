import * as actions from "./lobbyActions";
import { LobbyActionTypes } from "./lobbyActionTypes";
import { CreateRoomPayload, LobbyRoomState } from "./lobbyStateTypes";

export const updateRoomList = (rooms: LobbyRoomState[]): LobbyActionTypes => ({
    type: actions.ON_UPDATE_ROOM_LIST,
    rooms,
});

export const activateSearch = (): LobbyActionTypes => ({
    type: actions.ACTIVATE_SEARCH,
});

export const deactivateSearch = (): LobbyActionTypes => ({
    type: actions.DEACTIVATE_SEARCH,
});

export const setSearchQuery = (query: string): LobbyActionTypes => ({
    type: actions.SET_SEARCH_QUERY,
    query,
});

export const joinLobby = (): LobbyActionTypes => ({
    type: actions.JOIN_LOBBY,
});

export const leaveLobby = (): LobbyActionTypes => ({
    type: actions.LEAVE_LOBBY,
});

export const createRoom = (roomParams: CreateRoomPayload): LobbyActionTypes => ({
    type: actions.CREATE_ROOM,
    roomParams,
});
