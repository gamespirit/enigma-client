import { RoomData } from "../room/roomStateTypes";

export interface LobbyState {
    rooms: LobbyRoomState[];
    searchQuery: string;
    isSearching: boolean;
    createdRoomId?: string;
}

export interface LobbyRoomState extends Omit<RoomData, "players" | "teams" | "me"> {
    playersJoined: number;
}
export type CreateRoomPayload = Omit<
    LobbyRoomState,
    "id" | "playersJoined" | "countDownStarted" | "timer"
>;
