import React, { FC, useState, useEffect, useRef } from "react";
import { Button } from "../../../buttons";

interface Props {
    onSubmit: (name: string) => void;
}

const RoomNameForm: FC<Props> = ({ onSubmit }) => {
    const [name, setName] = useState("");
    const inputEl = useRef<HTMLInputElement>(null);
    useEffect(() => {
        inputEl.current?.focus();
    });
    return (
        <form
            className="form"
            onSubmit={(e) => {
                e.preventDefault();
                onSubmit(name);
            }}
        >
            <label htmlFor="">Room name*</label>
            <input
                type="text"
                placeholder="Type a name.."
                required={true}
                ref={inputEl}
                value={name}
                className="room-name"
                onChange={(e) => setName(e.target.value.toUpperCase())}
            />
            <div className="page-buttons">
                <Button colorClass="primary" type="submit">
                    Next
                </Button>
            </div>
        </form>
    );
};

export default RoomNameForm;
