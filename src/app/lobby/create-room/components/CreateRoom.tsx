import React, { FC, useEffect, useState } from "react";
import CopyToClipboard from "react-copy-to-clipboard";
import { connect, ConnectedProps } from "react-redux";
import history from "../../../../services/history";
import { RootState } from "../../../../store/reducers";
import { Button } from "../../../buttons";
import { BigTitle, BorderedWindow } from "../../../window";
import { createRoom } from "../../lobbyActionCreators";
import "./CreateRoom.css";
import RoomNameForm from "./RoomNameForm";
import RoomParams from "./RoomParams";

interface CreateRoomWindowProps {
    close: () => void;
}

const connector = connect(
    (state: RootState, ownProps: CreateRoomWindowProps) => ({
        ...ownProps,
        createdRoomId: state.app.lobby.createdRoomId,
    }),
    { createRoom }
);

enum Page {
    NAME = "NAME",
    SETTINGS = "SETTINGS",
    LINK_JOIN = "LINK_JOIN",
}

type Props = ConnectedProps<typeof connector>;

const CreateRoom: FC<Props> = ({ close, createRoom, createdRoomId }) => {
    const [name, setName] = useState("");
    const [page, setPage] = useState(Page.NAME);
    const [copied, setCopied] = useState(false);
    const linkToCreatedRoom = `${window.location.host}/room/${createdRoomId}`;
    const joinCreatedRoom = () => {
        history.push(`/room/${createdRoomId}`);
    };
    const onNameSubmit = (name: string) => {
        setName(name);
        setPage(Page.SETTINGS);
    };
    useEffect(() => {
        document.body.style.overflow = "hidden";

        return () => {
            document.body.style.overflow = "";
        };
    }, []);
    return (
        <div className="create-room centerer">
            <BorderedWindow colorClass="neutral">
                <div className="title-wrapper">
                    <BigTitle>New room</BigTitle>
                </div>
                <div
                    className="create-room-page"
                    style={page === Page.NAME ? undefined : { display: "none" }}
                >
                    <RoomNameForm onSubmit={onNameSubmit} />
                </div>
                <div
                    className="create-room-page"
                    style={page === Page.SETTINGS ? undefined : { display: "none" }}
                >
                    <RoomParams
                        onBack={() => {
                            setPage(Page.NAME);
                        }}
                        onNext={(rounds, numberOfTeams, playersPerTeam) => {
                            createRoom({ name, numberOfTeams, playersPerTeam, rounds });
                            setPage(Page.LINK_JOIN);
                        }}
                    />
                </div>
                <div
                    className="create-room-page"
                    style={
                        page === Page.LINK_JOIN && createdRoomId ? undefined : { display: "none" }
                    }
                >
                    <div className="link-page-text">Share the room link with your friends!</div>
                    <CopyToClipboard text={linkToCreatedRoom} onCopy={() => setCopied(true)}>
                        <div className="color-primary link-page-text">
                            Click on the link to copy it:
                        </div>
                    </CopyToClipboard>
                    <CopyToClipboard text={linkToCreatedRoom} onCopy={() => setCopied(true)}>
                        <div className={"room-link" + (copied ? " copied color-primary" : "")}>
                            {linkToCreatedRoom}
                            {copied ? <span className="icon sprite-tick-primary"></span> : null}
                        </div>
                    </CopyToClipboard>
                    <div className="page-buttons">
                        <Button
                            colorClass="primary"
                            onClick={() => {
                                joinCreatedRoom();
                            }}
                        >
                            Join room
                        </Button>
                    </div>
                </div>
                <button className="button-round neutral close-button" onClick={close}>
                    <div className="icon x-icon"></div>
                </button>
            </BorderedWindow>
        </div>
    );
};

export default connector(CreateRoom);
