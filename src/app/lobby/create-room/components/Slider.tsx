import React, { FC, useRef, useEffect, useState } from "react";
import "./Slider.css";

interface Props {
    values: number[];
    initialIndex: number;
    onChange: (value: number) => void;
}

const Slider: FC<Props> = ({ values, onChange, initialIndex }) => {
    const button = useRef<HTMLDivElement>(null);
    const position = useRef<HTMLDivElement>(null);
    const slider = useRef<HTMLDivElement>(null);
    const [snapIndex, setSnapIndex] = useState(initialIndex);
    useEffect(() => {
        const body = document.body;
        const sliderEl = slider.current!;
        let percent: number;
        const closest = (percent: number): { percent: number; index: number } => {
            const unit = 100 / (values.length - 1);
            const snapValues = new Array(values.length).fill(0).map((_, i) => unit * i);
            const closest = snapValues.reduce<{ percent: number; index: number }>(
                (prev, curr, i) => {
                    const diff = Math.abs(percent - curr);
                    const prevdiff = Math.abs(percent - prev.percent);
                    return diff < prevdiff ? { percent: curr, index: i } : prev;
                },
                { percent: 0, index: 0 }
            );
            return closest;
        };
        const move = (e: MouseEvent | TouchEvent) => {
            const sliderWidth = sliderEl.offsetWidth || 0;
            const sliderLeft = sliderEl.getBoundingClientRect().left || 0;
            let x: number;
            if (e instanceof MouseEvent) {
                x = e.x - sliderLeft;
            } else {
                x = e.touches[0].clientX - sliderLeft;
            }
            percent = Math.max(0, Math.min(100, (x / sliderWidth) * 100));
            position.current!.style.left = percent + "%";
            const c = closest(percent);
            setSnapIndex(c.index);
        };
        const release = () => {
            body.removeEventListener("mousemove", move);
            body.removeEventListener("touchmove", move);
            const c = closest(percent);
            position.current!.style.left = c.percent + "%";
            setSnapIndex(c.index);
            onChange(values[c.index]);
        };
        const grab = (e: MouseEvent | TouchEvent) => {
            body.addEventListener("mousemove", move);
            body.addEventListener("touchmove", move);
            body.addEventListener("touchend", release);
            body.addEventListener("mouseup", release);
            move(e);
        };
        sliderEl.addEventListener("touchstart", grab);
        sliderEl.addEventListener("mousedown", grab);
        return () => {
            body.removeEventListener("mousemove", move);
            body.removeEventListener("touchmove", move);
            sliderEl.removeEventListener("touchstart", grab);
            sliderEl.removeEventListener("mousedown", grab);
            body.removeEventListener("touchend", release);
            body.removeEventListener("mouseup", release);
        };
    }, [values, onChange]);
    return (
        <div className="slider">
            <div className="bar-wrapper">
                <div className="bar"></div>
            </div>
            <div className="slider-content" ref={slider}>
                <div className="markers">
                    {values.map((value, i) => (
                        <div className={"marker" + (snapIndex === i ? " snap" : "")} key={i}>
                            <div className="line"></div>
                            <div className="value">{value}</div>
                        </div>
                    ))}
                </div>
                <div
                    className="slider-position"
                    ref={position}
                    style={{ left: (100 / (values.length - 1)) * initialIndex + "%" }}
                >
                    <div className="slider-button sprite-slider-button" ref={button}></div>
                </div>
            </div>
        </div>
    );
};

export default Slider;
