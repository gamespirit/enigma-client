import React, { FC, useState } from "react";
import Slider from "./Slider";
import { Button } from "../../../buttons";
import TeamComposition from "../../../room/components/TeamComposition";
import RoundsIcon from "../../../room/components/RoundsIcon";

interface Props {
    onBack: () => void;
    onNext: (rounds: number, numberOfTeams: number, playersPerTeam: number) => void;
}

const RoomParams: FC<Props> = ({ onNext, onBack }) => {
    const [rounds, setRounds] = useState(3);
    const [numberOfTeams, setTeams] = useState(3);
    const [playersPerTeam, setPlayers] = useState(3);
    const [clicked, setClicked] = useState(false);
    return (
        <>
            <form className="form">
                <div className="form-grid">
                    <label>Rounds</label>
                    <Slider values={[1, 2, 3, 4, 5]} onChange={setRounds} initialIndex={2} />
                    <label>Number of teams</label>
                    <Slider values={[1, 2, 3]} onChange={setTeams} initialIndex={2} />
                    <label>Players per team</label>
                    <Slider values={[1, 2, 3]} onChange={setPlayers} initialIndex={2} />
                </div>
            </form>
            <div>
                <TeamComposition numberOfTeams={numberOfTeams} playersPerTeam={playersPerTeam} />
                <RoundsIcon>{rounds}</RoundsIcon>
            </div>
            <div className="page-buttons">
                <Button colorClass="neutral" onClick={() => onBack()}>
                    Back
                </Button>
                <Button
                    colorClass="primary"
                    busy={clicked}
                    busyText="Next"
                    onClick={() => {
                        setClicked(true);
                        onNext(rounds, numberOfTeams, playersPerTeam);
                    }}
                >
                    Next
                </Button>
            </div>
        </>
    );
};

export default RoomParams;
