import React, { FC, useEffect, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import accents from "remove-accents";
import { Button } from "../../buttons";
import { logOut } from "../../session";
import FixedHeader from "./FixedHeader";
import "./Lobby.css";
import LobbyRoom from "./LobbyRoom";
import RoomSearch from "./RoomSearch";
import { CreateRoom } from "../create-room";
import RoomError from "./RoomError";
import { RootState } from "../../../store/reducers";
import { joinLobby, leaveLobby } from "../lobbyActionCreators";

const connector = connect(
    (state: RootState) => ({
        user: state.app.session,
        rooms: state.app.lobby.rooms,
        query: state.app.lobby.searchQuery,
        isSearching: state.app.lobby.isSearching,
        roomError: state.app.room.error,
    }),
    { logOut, joinLobby, leaveLobby }
);

type Props = ConnectedProps<typeof connector>;

const Lobby: FC<Props> = ({ user, query, isSearching, rooms, logOut, joinLobby, leaveLobby }) => {
    const [createRoom, setCreateRoom] = useState(false);
    const roomsFiltered = rooms.filter((room) =>
        accents
            .remove(room.name.toLocaleLowerCase())
            .includes(accents.remove(query.toLocaleLowerCase()))
    );

    useEffect(() => {
        joinLobby();
        return () => {
            leaveLobby();
        };
    }, [joinLobby, leaveLobby]);

    return (
        <>
            <div className={`lobby${isSearching ? " search" : ""}`}>
                <FixedHeader>
                    <button className="button-round neutral logout-button" onClick={() => logOut()}>
                        <div className="icon sprite-exit-icon"></div>
                    </button>
                    <h1 className="lobby-title">Rooms</h1>
                    <Button
                        colorClass="primary"
                        extraClass="create-room-button"
                        onClick={() => setCreateRoom(true)}
                    >
                        Create a room
                    </Button>
                    <RoomSearch />
                </FixedHeader>
                <RoomError />
                <div className="rooms">
                    {roomsFiltered.map((room) => (
                        <LobbyRoom {...room} key={room.id} />
                    ))}
                </div>
            </div>
            {createRoom ? <CreateRoom close={() => setCreateRoom(false)} /> : null}
        </>
    );
};

export default connector(Lobby);
