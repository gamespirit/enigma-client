import React, { FC, useState, useEffect } from "react";
import "./FixedHeader.css";

const FixedHeader: FC = ({ children }) => {
    const [isFixedHeader, setIsFixedHeader] = useState(false);
    useEffect(() => {
        const checkViewport = () => {
            const scrollTop = document.documentElement.scrollTop;
            if (!isFixedHeader && scrollTop > 0) {
                setIsFixedHeader(true);
            } else if (isFixedHeader && scrollTop < 1) {
                setIsFixedHeader(false);
            }
        };
        window.addEventListener("scroll", checkViewport);
        checkViewport();
        return () => {
            window.removeEventListener("scroll", checkViewport);
        };
    }, [isFixedHeader]);
    return (
        <header className={`lobby-header${isFixedHeader ? " fixed" : ""}`}>
            <div className="header-content">
                <div className="header-content-inner">{children}</div>
            </div>
        </header>
    );
};

export default FixedHeader;
