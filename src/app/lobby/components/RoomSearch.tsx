import React, { FC, useRef, useEffect } from "react";
import "./RoomSearch.css";
import { ConnectedProps, connect } from "react-redux";
import { setSearchQuery, activateSearch, deactivateSearch } from "../lobbyActionCreators";
import { RootState } from "../../../store/reducers";

const connector = connect(
    (state: RootState) => ({
        query: state.app.lobby.searchQuery,
        isSearching: state.app.lobby.isSearching,
    }),
    {
        setSearchQuery,
        activateSearch,
        deactivateSearch,
    }
);

type Props = ConnectedProps<typeof connector>;

const RoomSearch: FC<Props> = ({
    query,
    isSearching,
    setSearchQuery,
    activateSearch,
    deactivateSearch,
}) => {
    const searchInputEl = useRef<HTMLInputElement>(null);
    const cancelSearch = () => {
        deactivateSearch();
    };
    useEffect(() => {
        if (isSearching) {
            searchInputEl.current?.focus();
        }
    }, [isSearching]);
    return (
        <>
            <button
                className="button-round neutral search-room-button"
                onClick={() => {
                    activateSearch();
                }}
            >
                <div className="icon sprite-search-icon"></div>
            </button>
            <button className="button-round primary cancel-search" onClick={cancelSearch}>
                <div className="icon x-icon"></div>
            </button>
            <input
                type="text"
                placeholder="Search for a room.."
                className="room-search-input"
                value={query}
                onChange={(e) => setSearchQuery(e.target.value)}
                onKeyDown={(e) => {
                    if (e.key === "Escape") {
                        cancelSearch();
                    }
                }}
                ref={searchInputEl}
            />
        </>
    );
};

export default connector(RoomSearch);
