// @flow
import * as React from "react";
import { FC } from "react";
import { LobbyRoomState } from "../lobbyStateTypes";
import "./LobbyRoom.css";
import { Button } from "../../buttons";
import { BorderedWindow, BigTitle } from "../../window";
import history from "../../../services/history";
import TeamComposition from "../../room/components/TeamComposition";
import RoomInfo from "../../room/components/RoomInfo";

const LobbyRoom: FC<LobbyRoomState> = (room) => {
    const maxPlayers = room.numberOfTeams * room.playersPerTeam;
    const isFull = room.playersJoined === maxPlayers;
    const name = room.name.length > 22 ? room.name.slice(0, 22).concat("..") : room.name;
    return (
        <div className={`lobby-room${isFull ? " full" : ""}`}>
            <BigTitle>{name}</BigTitle>
            <div className="room-content">
                <BorderedWindow colorClass="neutral">
                    <TeamComposition
                        numberOfTeams={room.numberOfTeams}
                        playersPerTeam={room.playersPerTeam}
                    />
                    <RoomInfo
                        playersJoined={room.playersJoined}
                        maxPlayers={maxPlayers}
                        rounds={room.rounds}
                    />
                    <Button
                        colorClass="primary"
                        busyText="Joining.."
                        disabled={isFull}
                        onClick={() => {
                            history.push(`/room/${room.id}`);
                        }}
                    >
                        {isFull ? "Full" : "Join"}
                    </Button>
                </BorderedWindow>
            </div>
        </div>
    );
};

export default LobbyRoom;
