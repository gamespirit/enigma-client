import React, { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../store/reducers";

const connector = connect((state: RootState) => ({ error: state.app.room.error }));

type Props = ConnectedProps<typeof connector>;

const RoomError: FC<Props> = ({ error }) => {
    return <>{error ? <span className="error">{error}</span> : null}</>;
};

export default connector(RoomError);
