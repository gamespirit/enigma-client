import { LobbyState } from "./lobbyStateTypes";
import * as actions from "./lobbyActions";
import { LobbyActionTypes } from "./lobbyActionTypes";
import produce from "immer";

const initialState: LobbyState = {
    rooms: [],
    isSearching: false,
    searchQuery: "",
};

export const lobbyReducer = produce((draft: LobbyState, action: LobbyActionTypes) => {
    switch (action.type) {
        case actions.CREATE_ROOM: {
            delete draft.createdRoomId;
            break;
        }
        case actions.ROOM_CREATED: {
            draft.createdRoomId = action.roomId;
            break;
        }
        case actions.ON_UPDATE_ROOM_LIST: {
            draft.rooms = action.rooms;
            break;
        }
        case actions.ACTIVATE_SEARCH: {
            draft.isSearching = true;
            break;
        }
        case actions.DEACTIVATE_SEARCH: {
            draft.isSearching = false;
            draft.searchQuery = "";
            break;
        }
        case actions.SET_SEARCH_QUERY: {
            draft.searchQuery = action.query;
            break;
        }
    }
}, initialState);
