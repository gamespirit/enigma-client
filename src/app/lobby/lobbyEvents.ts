// socket events
export const JOIN_LOBBY = "LOBBY/JOIN";
export const LEAVE_LOBBY = "LOBBY/LEAVE";
export const UPDATE_ROOM_LIST = "LOBBY/UPDATE_ROOM_LIST";
export const CREATE_ROOM = "ROOM/CREATE";
export const ROOM_CREATED = "ROOM/CREATED";
