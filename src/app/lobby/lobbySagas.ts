import { EventChannel, eventChannel, Task } from "redux-saga";
import { call, cancel, fork, put, take } from "redux-saga/effects";
import { updateRoomList } from "./lobbyActionCreators";
import * as actions from "./lobbyActions";
import { CreateRoomAction, LobbyActionTypes, RoomCreatedAction } from "./lobbyActionTypes";
import * as events from "./lobbyEvents";
import * as lobbyEvents from "./lobbyEvents";
import { LobbyRoomState } from "./lobbyStateTypes";

export function* lobbyFlow(socket: SocketIOClient.Socket) {
    while (true) {
        yield take(actions.JOIN_LOBBY);
        const roomList: Task = yield fork(watchRoomList, socket);
        const createRoom: Task = yield fork(watchCreateRoom, socket);
        yield take(actions.LEAVE_LOBBY);
        yield cancel(roomList);
        yield cancel(createRoom);
    }
}

function roomListChannel(socket: SocketIOClient.Socket) {
    return eventChannel((emit) => {
        socket.on(lobbyEvents.UPDATE_ROOM_LIST, (rooms: LobbyRoomState[]) => {
            emit(updateRoomList(rooms));
        });
        socket.emit(lobbyEvents.JOIN_LOBBY);
        return () => {
            socket.off(lobbyEvents.UPDATE_ROOM_LIST);
            socket.emit(lobbyEvents.LEAVE_LOBBY);
        };
    });
}

function createRoomChannel(socket: SocketIOClient.Socket) {
    return eventChannel((emit) => {
        socket.on(events.ROOM_CREATED, (roomId: string) => {
            const roomCreatedAction: RoomCreatedAction = {
                type: actions.ROOM_CREATED,
                roomId,
            };
            emit(roomCreatedAction);
        });
        return () => {
            socket.off(events.ROOM_CREATED);
        };
    });
}

function* watchCreateRoom(socket: SocketIOClient.Socket) {
    const channel: EventChannel<RoomCreatedAction> = yield call(createRoomChannel, socket);
    try {
        while (true) {
            const action: CreateRoomAction = yield take(actions.CREATE_ROOM);
            console.log("lobby create room channel open");
            socket.emit(events.CREATE_ROOM, action.roomParams);
            yield put(yield take(channel));
        }
    } finally {
        console.log("lobby create room channel closed");
        channel.close();
    }
}

function* watchRoomList(socket: SocketIOClient.Socket) {
    const channel: EventChannel<LobbyActionTypes> = yield call(roomListChannel, socket);
    try {
        console.log("lobby room list channel open");
        while (true) {
            yield put(yield take(channel));
        }
    } finally {
        console.log("lobby room list channel closed");
        channel.close();
    }
}
