import React, { FC, useEffect } from "react";
import "../assets/css/reset.css";
import "../assets/css/variables.css";
import "../assets/css/fonts.css";
import "../assets/css/sprites.css";
import "../assets/css/icons.css";
import "./App.css";
import { LoadingScreen } from "../loading-screen";
import { Switch, Route, Redirect } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";
import Lobby from "../lobby/components/Lobby";
import screenfull from "screenfull";
import Room from "../room/components/Room";
import { Game } from "../../game";

const App: FC = () => {
    useEffect(() => {
        // enter fullscreen on first touch on mobile
        const fullScreenOnClick = () => {
            if (screenfull.isEnabled && !screenfull.isFullscreen) {
                screenfull.request().catch((reason: any) => {
                    console.log("Full screen request rejected.");
                });
            }
        };
        document.body.addEventListener("touchend", fullScreenOnClick);
    });
    return (
        <div className="app">
            <PrivateRoute>
                <Switch>
                    <Route path="/game/:gameId">
                        <Game />
                    </Route>
                    <Route path="/room/:roomId">
                        <Room />
                    </Route>
                    <Route path="/lobby">
                        <Lobby />
                    </Route>
                    <Route path="/">
                        <Redirect to="/lobby" />
                    </Route>
                </Switch>
            </PrivateRoute>
            <LoadingScreen />
        </div>
    );
};

export default App;
