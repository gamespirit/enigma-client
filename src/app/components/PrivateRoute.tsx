import React, { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../store/reducers";
import { LoginScreen } from "../login";

const connector = connect((state: RootState) => ({
    authenticated: state.app.session.tokenId,
    connected: state.app.session.connected,
    initialized: state.app.session.initialized,
}));

type Props = ConnectedProps<typeof connector>;

const PrivateRoute: FC<Props> = ({ authenticated, connected, initialized, children }) => {
    const loggedIn = authenticated && connected && initialized;
    return <>{loggedIn ? children : <LoginScreen />}</>;
};

export default connector(PrivateRoute);
