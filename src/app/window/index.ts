export { default as BorderedWindow } from "./components/BorderedWindow";
export { default as BigTitle } from "./components/BigTitle";
export { default as SmallTitle } from "./components/SmallTitle";
