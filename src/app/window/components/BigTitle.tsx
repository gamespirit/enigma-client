import React, { FC } from "react";
import "./BigTitle.css";

const BitTitle: FC = ({ children }) => {
    return (
        <div className="big-title">
            <div className="sprite-title-big-side title-side left"></div>
            <div className="sprite-title-big-side title-side right"></div>
            <div className="title-body-padding">
                <div className="title-body"></div>
            </div>
            <div className="title-text">{children}</div>
        </div>
    );
};

export default BitTitle;
