import React, { FC } from "react";
import "./BorderedWindow.css";

interface Props {
    colorClass: string;
}

const BorderedWindow: FC<Props> = ({ colorClass, children }) => {
    return (
        <div className={"bordered-window color-" + colorClass}>
            <div className={"window-corner top left sprite-window-corner-" + colorClass}></div>
            <div className={"window-corner top right sprite-window-corner-" + colorClass}></div>
            <div className={"window-corner bottom left sprite-window-corner-" + colorClass}></div>
            <div className={"window-corner bottom right sprite-window-corner-" + colorClass}></div>
            <div className="window-side top"></div>
            <div className="window-side bottom"></div>
            <div className="window-side left"></div>
            <div className="window-side right"></div>
            {children}
        </div>
    );
};

export default BorderedWindow;
