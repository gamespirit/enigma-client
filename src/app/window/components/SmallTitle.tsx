import React, { FC } from "react";
import "./SmallTitle.css";

interface Props {
    color: string;
}

const SmallTitle: FC<Props> = ({ children, color }) => {
    return (
        <div className="small-title">
            <div className="title-bg-wrapper sprite-small-title-background">
                <div className={`title-bg sprite-small-title-${color}`}></div>
            </div>
            <div className="content">{children}</div>
        </div>
    );
};

export default SmallTitle;
