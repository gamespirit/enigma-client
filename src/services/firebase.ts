import firebase from "firebase/app";
import "firebase/analytics";
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBtSQBHAOHlYHkRtQwnP477f4VdCRbTydo",
    authDomain: "enigma-gamespirit.firebaseapp.com",
    databaseURL: "https://enigma-gamespirit.firebaseio.com",
    projectId: "enigma-gamespirit",
    storageBucket: "enigma-gamespirit.appspot.com",
    messagingSenderId: "548001966111",
    appId: "1:548001966111:web:2db5869ee0f66c7b308d2a",
    measurementId: "G-Y3ZNLEHL5C",
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Use browser language on facebook auth page
firebase.auth().useDeviceLanguage();

export const fbAuthProvider = new firebase.auth.FacebookAuthProvider();
