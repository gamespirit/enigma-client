import { EventChannel, eventChannel } from "redux-saga";
import { call, fork, take } from "redux-saga/effects";

export interface SocketEventDescription {
    /** Event name */
    name: string;

    /** Extra arguments to pass to the event handler function */
    extraArgs?: any[];

    /** Called after an action is emitted */
    handler: (...args: any[]) => any;
}

interface Payload {
    event: SocketEventDescription;
    payload: any[];
}

export function* listenForSocketEvents(
    socket: SocketIOClient.Socket,
    events: SocketEventDescription[]
) {
    const channel: EventChannel<Payload> = yield call(() =>
        eventChannel<Payload>((emit) => {
            events.forEach((event) =>
                socket.on(event.name, (...args: any) => emit({ event, payload: args }))
            );

            return () => {
                events.forEach((event) => socket.off(event.name, event.handler));
            };
        })
    );
    try {
        while (true) {
            const emit: Payload = yield take(channel);
            if (emit.event.extraArgs) {
                yield fork(emit.event.handler, ...emit.event.extraArgs, ...emit.payload);
            } else {
                yield fork(emit.event.handler, ...emit.payload);
            }
        }
    } finally {
        console.log("listenForSocketEvents finished: " + events.map((e) => e.name).join(","));
        channel.close();
    }
}

export function* listenForSocketEventOnce(
    socket: SocketIOClient.Socket,
    event: SocketEventDescription
) {
    const channel: EventChannel<Payload> = yield call(() =>
        eventChannel((emit) => {
            socket.on(event.name, (...args: any) => emit(args));
            return () => {
                socket.off(event.name, event.handler);
            };
        })
    );
    try {
        const payload: any[] = yield take(channel);
        yield fork(event.handler, ...payload);
    } finally {
        console.log("listenForSocketEventOnce finished: " + event.name);
        channel.close();
    }
}

export function* takeSocketEventOnce(socket: SocketIOClient.Socket, eventName: string) {
    const channel: EventChannel<boolean> = yield call(() =>
        eventChannel((emit) => {
            const handler = () => emit(true);
            socket.on(eventName, handler);
            return () => {
                socket.off(eventName, handler);
            };
        })
    );
    try {
        yield take(channel);
    } finally {
        console.log("takeSocketEventOnce finished: " + eventName);
        channel.close();
    }
}
