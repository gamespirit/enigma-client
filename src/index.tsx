import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import store from "./store/store";
import { Router } from "react-router-dom";
import history from "./services/history";
import { App } from "./app";
import { sagaStore } from "./debug/sagaMonitor";
import SagaDebugComponent from "./debug/SagaDebugComponent";

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Router history={history}>
                <App />
            </Router>
        </Provider>
        <Provider store={sagaStore}>
            <SagaDebugComponent />
        </Provider>
    </React.StrictMode>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
