import { applyMiddleware, createStore, StoreEnhancer } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import thunk from "redux-thunk";
import { sagaMonitor } from "../debug/sagaMonitor";
import { rootReducer } from "./reducers";
import { rootSaga } from "./rootSaga";

let enhancer: StoreEnhancer<{ dispatch: unknown }, {}>;

const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

if (process.env.NODE_ENV === "development") {
    enhancer = composeWithDevTools(applyMiddleware(thunk, sagaMiddleware));
} else {
    enhancer = applyMiddleware(thunk, sagaMiddleware);
}

const store = createStore(rootReducer, enhancer);

sagaMiddleware.run(rootSaga);

export default store;
