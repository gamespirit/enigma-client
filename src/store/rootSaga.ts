import { call, cancel, fork, put, race } from "redux-saga/effects";
import { lobbyFlow } from "../app/lobby/lobbySagas";
import { roomFlow } from "../app/room/roomSagas";
import { initializeApp } from "../app/session";
import {
    createSession,
    loadSession,
    watchDisconnect,
    watchLogout,
} from "../app/session/sessionSagas";
import { gameFlow } from "../game/gameSagas";

export function* rootSaga() {
    // check if user is logged in
    yield call(loadSession);
    while (true) {
        // wait until the user is logged in and connected
        const socket = yield call(createSession); // connect instead
        const appTasks = yield fork(appSaga, socket);
        yield put(initializeApp());

        // if there is a disconnect or logout, cancel any other tasks currently running
        // and start the flow over again with login
        yield race({
            onDisconnect: call(watchDisconnect, socket),
            onLogOut: call(watchLogout, socket),
        });
        yield cancel(appTasks);
    }
}

function* appSaga(socket: SocketIOClient.Socket) {
    try {
        yield fork(lobbyFlow, socket);
        yield fork(roomFlow, socket);
        yield fork(gameFlow, socket);
    } finally {
        console.log("appSaga cancelled!");
    }
}
