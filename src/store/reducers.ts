import { combineReducers } from "redux";
import { appReducer } from "../app";
import { gameReducer } from "../game";

export const rootReducer = combineReducers({
    app: appReducer,
    game: gameReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
