/** EDIT THESE TO MATCH PROJECT */
const svgDir = "./src/app/assets/svg"; // path to svg fragments directory
const outDir = "./src/app/assets/css"; // will output sprite.css and sprite.svg to this location
const cssTemplate = "./tools/template.css";

const mustache = require("mustache");
const fs = require("fs");
const mkdirp = require("mkdirp");
const path = require("path");

function priority(fileName) {
    const aFocus = fileName.indexOf("-focus") !== -1 ? 4 : 0;
    const aActive = fileName.indexOf("-active") !== -1 ? 3 : 0;
    const aHover = fileName.indexOf("-hover") !== -1 ? 2 : 0;
    const aVisited = fileName.indexOf("-visited") !== -1 ? 1 : 0;
    return aFocus + aActive + aHover + aVisited;
}

fs.readdir(svgDir, function (err, filenames) {
    const svgs = [];

    for (const file of filenames) {
        if (path.extname(file) === ".svg") {
            const svg = `${svgDir}/${file}`;
            console.log(svg);
            svgs.push({
                name: file.slice(0, file.indexOf(path.extname(file))),
                uri: getConvertedURI(fs.readFileSync(svg, { encoding: "utf-8" })),
            });
        }
    }

    svgs.sort((a, b) => {
        const compAlpahbetical = a < b ? -1 : a > b ? 1 : 0;
        if (compAlpahbetical === 0) {
            const prioA = priority(a.name);
            const prioB = priority(b.name);
            return prioA - prioB;
        }
        return compAlpahbetical;
    });

    const svgsFinal = [];
    for (const svg of svgs) {
        svgsFinal.push(svg);
        if (svg.name.includes("-focus")) {
            svgsFinal.push({
                name: svg.name.replace("-focus", ":focus"),
                uri: svg.uri,
            });
        }
        if (svg.name.includes("-hover")) {
            svgsFinal.push({
                name: svg.name.replace("-hover", ":hover"),
                uri: svg.uri,
            });
        }
        if (svg.name.includes("-active")) {
            svgsFinal.push({
                name: svg.name.replace("-active", ":active"),
                uri: svg.uri,
            });
        }
        if (svg.name.includes("-visited")) {
            svgsFinal.push({
                name: svg.name.replace("-visited", ":visited"),
                uri: svg.uri,
            });
        }
    }

    const template = fs.readFileSync(cssTemplate, { encoding: "utf-8" });
    const output = mustache.render(template, { svgs: svgsFinal });

    mkdirp.sync(outDir);
    fs.writeFileSync(path.join(outDir, "sprites.css"), output);
});

const quotes = {
    level1: `"`,
    level2: `'`,
};

function encodeSVG(data) {
    data = data.replace(/"/g, "'");

    data = data.replace(/>\s{1,}</g, "><");
    data = data.replace(/\s{2,}/g, " ");

    const symbols = /[\r\n%#()<>?\[\\\]^`{|}]/g;
    return data.replace(symbols, encodeURIComponent);
}

function addNameSpace(data) {
    if (data.indexOf("http://www.w3.org/2000/svg") < 0) {
        data = data.replace(
            /<svg/g,
            `<svg xmlns=${quotes.level2}http://www.w3.org/2000/svg${quotes.level2}`
        );
    }

    return data;
}

function getConvertedURI(svg) {
    var namespaced = addNameSpace(svg);
    var escaped = encodeSVG(namespaced);
    return `${quotes.level1}data:image/svg+xml,${escaped}${quotes.level1}`;
}
